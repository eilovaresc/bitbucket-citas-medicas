package com.api.citasmedicas.util;

/**
 * Created by Usuario on 9/11/2020.
 */
public class MensajeRespuesta {
    private boolean success;
    private String mensajes;


    public MensajeRespuesta(boolean success, String mensajes) {
        this.success = success;
        this.mensajes = mensajes;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMensajes() {
        return mensajes;
    }

    public void setMensajes(String errors) {
        this.mensajes = mensajes;
    }
}
