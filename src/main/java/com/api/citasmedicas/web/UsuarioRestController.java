package com.api.citasmedicas.web;

import com.api.citasmedicas.domain.Usuario;
import com.api.citasmedicas.service.UsuarioService;
import com.api.citasmedicas.util.MensajeRespuesta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Usuario on 15/11/2020.
 */
@RestController
public class UsuarioRestController {


    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UsuarioService usuarioService;


    @RequestMapping(value = "/usuario/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<Usuario> all = usuarioService.findAll();
            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/usuario/clinica/{clinica}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllClinica(@PathVariable("clinica") String clinica, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<Usuario> all = usuarioService.findByRol(clinica);
            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/usuario/rol/{rol}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllRol(@PathVariable("rol") String rol, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<Usuario> all = usuarioService.findByRol(rol);
            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/usuario/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUsuarioPerfil(@RequestBody Usuario usuario, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            Usuario userLogin = usuarioService.loginUser(usuario);
            if (userLogin != null) {
                return new ResponseEntity(userLogin, HttpStatus.OK);
            } else {
                return new ResponseEntity(null, HttpStatus.UNAUTHORIZED);
            }
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado"), HttpStatus.BAD_REQUEST);
    }
}
