package com.api.citasmedicas.web;

import com.api.citasmedicas.domain.DiagnosticosFotosDoctorDet;
import com.api.citasmedicas.domain.DiagnosticosfotosDoctor;
import com.api.citasmedicas.domain.ViewDiagnosticoFotosDoctor;
import com.api.citasmedicas.service.DiagnosticoFotosDoctorService;
import com.api.citasmedicas.util.MensajeRespuesta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Created by Usuario on 3/7/2021.
 */
@RestController
public class DiagnosticoFotosDoctorRestController {

    @Autowired
    private DiagnosticoFotosDoctorService fotosService;


    @RequestMapping(value = "/diagnostico-fotos/medico/upload", method = RequestMethod.POST)
    public ResponseEntity<?> handleFileUpload(@RequestParam("documento") MultipartFile fileDerecho, HttpServletRequest request) {
        String fecha = request.getHeader("x-fecha-proceso");
        String hora = request.getHeader("x-hora");
        String pacienteId = request.getHeader("x-paciente-id");

        String usuario = request.getHeader("x-user");


        DiagnosticosfotosDoctor fotoInsertada = this.insertarNuevaCab(usuario, pacienteId, fecha, hora);

        try {
            insertatDet(fileDerecho, fotoInsertada);

            return new ResponseEntity(new MensajeRespuesta(true, "Archivo ok"), HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity(new MensajeRespuesta(false, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/diagnostico-fotos/medico/paciente/{paciente}/{fechaProceso}/{horaCita}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllFotosPaciente(@PathVariable("paciente") String pacienteId,
                                                 @PathVariable("fechaProceso") String fechaProceso,
                                                 @PathVariable("horaCita") String horaCita,
                                                 HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<ViewDiagnosticoFotosDoctor> all =
                    this.fotosService.findViewByPacienteIdFechaProcesHoraCita(pacienteId, fechaProceso, horaCita);


            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/diagnostico-fotos/medico/{detalleId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getFotosPaciente(@PathVariable("detalleId") Long detalleId,
                                              HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {
            ViewDiagnosticoFotosDoctor detalle = this.fotosService.getbyDetalleId(detalleId);

            return new ResponseEntity(detalle, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    DiagnosticosfotosDoctor insertarNuevaCab(String usuario, String pacienteId, String fecha, String hora) {
        DiagnosticosfotosDoctor fotoInsertada = null;
        DiagnosticosfotosDoctor diagnosticosfotos = new DiagnosticosfotosDoctor();
        diagnosticosfotos.setPacienteId(pacienteId);
        diagnosticosfotos.setUsuarioRegistro(usuario);
        diagnosticosfotos.setMdFechaProceso(fecha);
        diagnosticosfotos.setMdHoraCita(hora);


        fotoInsertada = fotosService.save(diagnosticosfotos);
        return fotoInsertada;
    }

    void insertatDet(MultipartFile file, DiagnosticosfotosDoctor cab) throws IOException {
        byte[] fileContent = null;


        if (!file.isEmpty()) {
            fileContent = file.getBytes();
        }

        DiagnosticosFotosDoctorDet detalle = new DiagnosticosFotosDoctorDet();
        detalle.setFotoId((int) cab.getFotoId());
        detalle.setFoto(fileContent);
        String fileName = file.getOriginalFilename().toUpperCase();
        detalle.setNombreImagen(fileName);
        detalle.setTipo(fileName.contains(".PDF") ? "PDF" : "IMAGEN");

        this.fotosService.saveDetalle(detalle);
    }

}
