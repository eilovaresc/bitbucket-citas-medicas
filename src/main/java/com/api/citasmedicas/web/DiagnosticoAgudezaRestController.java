package com.api.citasmedicas.web;

import com.api.citasmedicas.domain.DiagnosticoAgudeza;
import com.api.citasmedicas.service.DiagnosticoAgudezaService;
import com.api.citasmedicas.util.MensajeRespuesta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Usuario on 30/11/2020.
 */
@RestController
public class DiagnosticoAgudezaRestController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DiagnosticoAgudezaService service;


    @RequestMapping(value = "/diagnostico-agudeza/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<DiagnosticoAgudeza> all = this.service.findAll();

            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado"), HttpStatus.BAD_REQUEST);
    }
}
