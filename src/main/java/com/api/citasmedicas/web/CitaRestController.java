package com.api.citasmedicas.web;

import com.api.citasmedicas.domain.CitaPaciente;
import com.api.citasmedicas.domain.ViewCitaPaciente;
import com.api.citasmedicas.service.CitaPacienteService;
import com.api.citasmedicas.util.MensajeRespuesta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Usuario on 15/11/2020.
 */
@RestController
public class CitaRestController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CitaPacienteService citaPacienteService;

    @RequestMapping(value = "/cita-paciente/agendar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveCitaPaciente(@RequestBody CitaPaciente citaPaciente,
                                              HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {
            this.saveCita(citaPaciente);

            return new ResponseEntity(new MensajeRespuesta(true, "Registro exitoso"), HttpStatus.OK);
        }
        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/cita-paciente/fecha-cita/{fechaCita}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllbyFechaCita(@PathVariable("fechaCita") String fechaCita, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");
        String clinicaId = request.getHeader("x-clinica-id");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<ViewCitaPaciente> all = this.citaPacienteService.findByFechaPlanifica(clinicaId, fechaCita);

            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }


    @RequestMapping(value = "/cita-paciente/medico/{medicoId}/{fechaCita}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllbyClinicaIdMedicoIdFechaCita(@PathVariable("fechaCita") String fechaCita,
                                                                @PathVariable("medicoId") String medicoId, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");
        String clinicaId = request.getHeader("x-clinica-id");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<ViewCitaPaciente> all = this.citaPacienteService.findByClinicaIdMedicoIdFechaPlanifica(clinicaId, medicoId, fechaCita);

            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/cita-paciente/registro/{fechaCita}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllbyRegistroClinicaIdFechaCita(@PathVariable("fechaCita") String fechaCita, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");
        String clinicaId = request.getHeader("x-clinica-id");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<ViewCitaPaciente> all = this.citaPacienteService.findByClinicaIdFechaPlanifica(clinicaId, fechaCita);

            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    void saveCita(CitaPaciente citaPaciente) {

        citaPaciente.setEstado("A");
        this.citaPacienteService.save(citaPaciente);

    }

}

