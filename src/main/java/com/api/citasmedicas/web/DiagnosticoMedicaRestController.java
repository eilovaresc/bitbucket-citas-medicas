package com.api.citasmedicas.web;

import com.api.citasmedicas.domain.CitaPaciente;
import com.api.citasmedicas.domain.DiagnosticoMedico;
import com.api.citasmedicas.domain.ViewDiagnosticoMedico;
import com.api.citasmedicas.service.CitaPacienteService;
import com.api.citasmedicas.service.DiagnisticoMedicoService;
import com.api.citasmedicas.util.MensajeRespuesta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Usuario on 26/11/2020.
 */
@RestController
public class DiagnosticoMedicaRestController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DiagnisticoMedicoService diagnisticoMedicoService;
    @Autowired
    private CitaPacienteService citaPacienteService;

    @RequestMapping(value = "/diagnostico-medico", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveDiagnostico(@RequestBody DiagnosticoMedico diagnosticoMedico,
                                             HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");
        String horaCita = request.getHeader("x-hora-cita");
        try {
            String clinicaId = request.getHeader("x-clinica-id");
            if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {


                DiagnosticoMedico existente = this.diagnisticoMedicoService.findById(diagnosticoMedico.getMdKeyPacienteId(),
                        diagnosticoMedico.getMdFechaProceso(), horaCita);
                String horaProceso = existente == null ? diagnosticoMedico.getMdHoraProceso() : existente.getMdHoraProceso();
                existente = diagnosticoMedico;

                existente.setMdHoraProceso(horaProceso);
                if ("F".equalsIgnoreCase(diagnosticoMedico.getMdEstadoDiagnostico())) {
                    Calendar c = Calendar.getInstance();

                    String horaProcesoFin =
                            (c.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + c.get(Calendar.HOUR_OF_DAY) : c.get(Calendar.HOUR_OF_DAY)) + ":"
                                    + (c.get(Calendar.MINUTE) < 10 ? "0" + c.get(Calendar.MINUTE) : c.get(Calendar.MINUTE)) + ":"
                                    + (c.get(Calendar.SECOND) < 10 ? "0" + c.get(Calendar.SECOND) : c.get(Calendar.SECOND));

                    diagnosticoMedico.setMdHoraProcesoFin(horaProcesoFin);
                    actualizarEstadoCitaPaciente(diagnosticoMedico.getMdKeyPacienteId(), diagnosticoMedico.getMdFechaProceso(), horaCita);
                    this.diagnisticoMedicoService.save(existente);
                } else {
                    this.diagnisticoMedicoService.save(existente);
                }
                return new ResponseEntity(diagnosticoMedico, HttpStatus.OK);
            }
            return new ResponseEntity(new MensajeRespuesta(false, "No autorizado"), HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());

            return new ResponseEntity(new MensajeRespuesta(false, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/diagnostico-medico/paciente/{paciente}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllDiagnosticoPaciente(@PathVariable("paciente") String pacienteId, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");
        String clinicaId = request.getHeader("x-clinica-id");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<ViewDiagnosticoMedico> all = this.diagnisticoMedicoService.findAllByPacienteId(pacienteId);

            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/diagnostico-medico/paciente/query", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllDiagnosticoPaciente(@RequestParam(name = "nombre") String nombre,
                                                       @RequestParam(name = "pacienteId") String pacienteId,
                                                       HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");
        String clinicaId = request.getHeader("x-clinica-id");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {
            List<ViewDiagnosticoMedico> all = null;
            if ("TODAS".equalsIgnoreCase(clinicaId)) {
                all = this.diagnisticoMedicoService.findAllByNombreOrPacienteId(nombre, pacienteId);
            } else {
                all = this.diagnisticoMedicoService.findAllByNombreOrPacienteIdAndClinicaId(nombre, pacienteId, clinicaId);
            }
            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/diagnostico-medico/paciente/{paciente}/fecha/{fechaProceso}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getDiagnostico(@PathVariable("paciente") String pacienteId, @PathVariable("fechaProceso") String fechaProceso, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");
        String clinicaId = request.getHeader("x-clinica-id");
        String horaCita = request.getHeader("x-hora-cita");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            DiagnosticoMedico diagnosticoMedico = this.diagnisticoMedicoService.findById(pacienteId, fechaProceso, horaCita);

            return new ResponseEntity(diagnosticoMedico, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }


    void actualizarEstadoCitaPaciente(String pacienteId, String fechaPlanifica, String horaPlanifica) {

        CitaPaciente citaActual = this.citaPacienteService.findByPacienteIdAndFechaPlanificaAndHoraPlanifica(pacienteId, fechaPlanifica, horaPlanifica);
        citaActual.setEstado("F");
        this.citaPacienteService.save(citaActual);
    }
}
