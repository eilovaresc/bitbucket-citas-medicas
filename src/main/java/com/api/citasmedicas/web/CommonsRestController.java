package com.api.citasmedicas.web;

import com.api.citasmedicas.domain.Paciente;
import com.api.citasmedicas.util.MensajeRespuesta;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;

/**
 * Created by Usuario on 3/3/2021.
 */
@RestController
public class CommonsRestController {

    @RequestMapping(value = "/commons/version", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getVersion() {
        return new ResponseEntity(new MensajeRespuesta(true,
                "Servios activos, version 0.1.0"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/commons/heartbeat", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> heartbeat() {

        return new ResponseEntity("Servidor activo v 1.1", HttpStatus.OK);
    }

    @RequestMapping(value = "/paciente/dummy", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> dummy() {
        Paciente p = new Paciente();
        p.setDepartamentoId("1");
        p.setDireccion("direccion");
        p.setEstadoCivil("C");
        p.setFechaNacimiento(Calendar.getInstance().getTime());
        p.setFechaRegistro(Calendar.getInstance().getTime());
        p.setKeyDpiId("123456");
        p.setPacienteId("210300001");
        p.setProgramaInserta("sp");
        p.setSexo("F");
        p.setTelefono("32998162");
        p.setTipoSangre("A+");
        p.setUsuarioId("edwini");

        return new ResponseEntity(p, HttpStatus.OK);
    }

}
