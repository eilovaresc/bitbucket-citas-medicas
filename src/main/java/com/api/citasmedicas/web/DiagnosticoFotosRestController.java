package com.api.citasmedicas.web;

import com.api.citasmedicas.domain.DiagnosticosFotosDet;
import com.api.citasmedicas.domain.Diagnosticosfotos;
import com.api.citasmedicas.domain.ViewDiagnosticoFotos;
import com.api.citasmedicas.service.DiagnosticoFotosService;
import com.api.citasmedicas.util.MensajeRespuesta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Usuario on 3/7/2021.
 */
@RestController
public class DiagnosticoFotosRestController {

    @Autowired
    private DiagnosticoFotosService fotosService;

    @RequestMapping(value = "/diagnostico-fotos/detalle/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deteleFotoDet(@RequestBody DiagnosticosFotosDet foto,
                                           HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {
            this.fotosService.deleteFotoDet(foto);

            return new ResponseEntity(new MensajeRespuesta(true, "Registro eliminado"), HttpStatus.OK);
        }
        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/diagnostico-fotos/paciente/{paciente}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllFotosPaciente(@PathVariable("paciente") String pacienteId, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<ViewDiagnosticoFotos> all = new ArrayList<>();
            all = this.fotosService.findViewByPacienteId(pacienteId);


            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/diagnostico-fotos/upload/derecho", method = RequestMethod.POST)
    public ResponseEntity<?> handleFileUpload2(@RequestParam("derecho") MultipartFile fileDerecho, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");
        String pacienteId = request.getHeader("x-paciente-id");
        String obsDere = request.getHeader("x-obs-dere");
        String obsIzq = request.getHeader("x-obs-izq");

        String usuario = request.getHeader("x-user");
        String ojo = "DERECHO";


        List<Diagnosticosfotos> diagnosticos = this.fotosService.findAllByPacienteId(pacienteId);

        Diagnosticosfotos fotoInsertada = this.insertarNuevaCab(diagnosticos, usuario, pacienteId, obsDere, obsIzq);

        try {
            insertatDet(fileDerecho, fotoInsertada, ojo);

            return new ResponseEntity(new MensajeRespuesta(false, "Archivo ok"), HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity(new MensajeRespuesta(false, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/diagnostico-fotos/upload/izquierdo", method = RequestMethod.POST)
    public ResponseEntity<?> handleFileUpload(@RequestParam("izquierdo") MultipartFile fileIzquierdo, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");
        String pacienteId = request.getHeader("x-paciente-id");
        String obsDere = request.getHeader("x-obs-dere");
        String obsIzq = request.getHeader("x-obs-izq");
        String usuario = request.getHeader("x-user");
        String ojo = "IZQUIERDO";

        List<Diagnosticosfotos> diagnosticos = this.fotosService.findAllByPacienteId(pacienteId);
        Diagnosticosfotos fotoInsertada = this.insertarNuevaCab(diagnosticos, usuario, pacienteId, obsDere, obsIzq);

        try {
            insertatDet(fileIzquierdo, fotoInsertada, ojo);

            return new ResponseEntity(new MensajeRespuesta(false, "Archivo ok"), HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity(new MensajeRespuesta(false, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    Diagnosticosfotos insertarNuevaCab(List<Diagnosticosfotos> diagnosticos, String usuario, String pacienteId,
                                       String obsDere,
                                       String obsIzq) {
        Diagnosticosfotos fotoInsertada = null;
        Diagnosticosfotos diagnosticosfotos = new Diagnosticosfotos();
        if (diagnosticos.size() > 0) {
            diagnosticosfotos = diagnosticos.get(0);
            diagnosticosfotos.setObservacionesDerecha(obsDere);
            diagnosticosfotos.setObservacionesIzquierda(obsIzq);
        } else {
            diagnosticosfotos.setObservacionesDerecha(obsDere);
            diagnosticosfotos.setObservacionesIzquierda(obsIzq);
            diagnosticosfotos.setPacienteId(pacienteId);
            diagnosticosfotos.setFechaRegistro(new Date());
            diagnosticosfotos.setUsuarioRegistro(usuario);
        }

        fotoInsertada = fotosService.save(diagnosticosfotos);
        return fotoInsertada;
    }

    void insertatDet(MultipartFile file, Diagnosticosfotos cab, String ojo) throws IOException {
        byte[] fileContent = null;


        if (!file.isEmpty()) {
            fileContent = file.getBytes();
        }

        DiagnosticosFotosDet detalle = new DiagnosticosFotosDet();
        detalle.setFotoId((int) cab.getFotoId());
        detalle.setFoto(fileContent);
        detalle.setOjo(ojo);
        String fileName = file.getOriginalFilename().toUpperCase();
        detalle.setNombreImagen(fileName);
        detalle.setTipo(fileName.contains(".PDF") ? "PDF" : "IMAGEN");

        this.fotosService.saveDetelle(detalle);
    }

}
