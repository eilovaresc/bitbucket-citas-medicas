package com.api.citasmedicas.web;

import com.api.citasmedicas.domain.ReportResumenControlPacientes;
import com.api.citasmedicas.service.ReportesService;
import com.api.citasmedicas.util.MensajeRespuesta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Usuario on 17/2/2022.
 */
@RestController
public class ReportesRestController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private ReportesService service;

    @RequestMapping(value = "/reportes/resumencontrolpacientes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAll(@RequestParam(name = "clinicaId") String clinicaId,
                                    @RequestParam(name = "desde") String desde,
                                    @RequestParam(name = "hasta") String hasta,
                                    HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {
            if ("TODAS".equalsIgnoreCase(clinicaId)) {
                clinicaId = null;
            }
            List<ReportResumenControlPacientes> all = this.service.reportResumenControlPacientes(desde, hasta, clinicaId);
            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado"), HttpStatus.BAD_REQUEST);
    }
}
