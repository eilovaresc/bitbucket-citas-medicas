package com.api.citasmedicas.web;

import com.api.citasmedicas.domain.CitaPaciente;
import com.api.citasmedicas.domain.Correlativo;
import com.api.citasmedicas.domain.CorrelativoPK;
import com.api.citasmedicas.domain.Paciente;
import com.api.citasmedicas.service.CitaPacienteService;
import com.api.citasmedicas.service.CorrelativoService;
import com.api.citasmedicas.service.PacienteService;
import com.api.citasmedicas.util.MensajeRespuesta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Usuario on 9/11/2020.
 */
@RestController
public class PacienteRestController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PacienteService pacienteService;

    @Autowired
    private CorrelativoService correlativoService;

    @Autowired
    private CitaPacienteService citaPacienteService;


    @RequestMapping(value = "/paciente", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addPaciente(@RequestBody Paciente paciente,
                                         HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {
            //  Clinica clinica = this.clinicaService.findByClienteId(paciente.getClinicaId());
            if (paciente.getPacienteId() == null) {
                this.generarPacienteId(paciente);
                this.pacienteService.save(paciente);
            } else {
                Paciente modificarPaciente = this.pacienteService.findByPacienteId(paciente.getPacienteId());
                modificarPaciente.setNombre(paciente.getNombre());
                modificarPaciente.setFechaNacimiento(paciente.getFechaNacimiento());
                modificarPaciente.setTipoSangre(paciente.getTipoSangre());
                modificarPaciente.setSexo(paciente.getSexo());
                modificarPaciente.setEstadoCivil(paciente.getEstadoCivil());
                modificarPaciente.setDepartamentoId(paciente.getDepartamentoId());
                modificarPaciente.setDireccion(paciente.getDireccion());
                modificarPaciente.setTelefono(paciente.getTelefono());
                modificarPaciente.setKeyDpiId(paciente.getKeyDpiId());
                this.pacienteService.save(modificarPaciente);
            }
            return new ResponseEntity(paciente, HttpStatus.OK);
        }
        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/paciente/{pacienteId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPacienteId(@PathVariable("pacienteId") String pacienteId, HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            Paciente paciente = this.pacienteService.findByPacienteId(pacienteId);
            if (paciente == null) {

                return new ResponseEntity(new Paciente(), HttpStatus.OK);
            }
            return new ResponseEntity(paciente, HttpStatus.OK);
        }
        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/paciente", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> queryPaciente(@RequestParam(name = "nombre") String nombre,
                                           HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<Paciente> pacientes = this.pacienteService.findByNombre(nombre);
            return new ResponseEntity(pacientes, HttpStatus.OK);
        }
        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/cita-paciente", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveCitaPaciente(@RequestBody CitaPaciente citaPaciente,
                                              HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            this.citaPacienteService.save(citaPaciente);

            return new ResponseEntity(new MensajeRespuesta(true, "Registro exitoso"), HttpStatus.OK);
        }
        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado exitoso"), HttpStatus.BAD_REQUEST);
    }


    private void generarPacienteId(Paciente paciente) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(paciente.getFechaRegistro());
        int anio = cal.get(Calendar.YEAR);
        int mes = cal.get(Calendar.MONTH) + 1;
        int correlativo = this.aumentoCorrelativo(anio, mes);
        StringBuilder pacienteID = new StringBuilder(String.valueOf(anio).substring(2))
                .append(mes > 9 ? mes : "0" + mes)
                .append(padLeftZeros(correlativo, 5));
        paciente.setPacienteId(pacienteID.toString());
    }

    private int aumentoCorrelativo(int anio, int mes) {
        CorrelativoPK pk = new CorrelativoPK();
        pk.setAnio(anio);
        pk.setMes(mes);
        Correlativo correlativo = this.correlativoService.findById(pk);
        if (correlativo != null) {
            correlativo.setCorrelativo(correlativo.getCorrelativo() + 1);
        } else {
            correlativo = new Correlativo();
            correlativo.setAnio(anio);
            correlativo.setMes(mes);
            correlativo.setCorrelativo(1);
        }
        this.correlativoService.save(correlativo);
        return correlativo.getCorrelativo();
    }

    public String padLeftZeros(int input, int length) {
        String inputString = String.valueOf(input);
        if (inputString.length() >= length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < length - inputString.length()) {
            sb.append('0');
        }
        sb.append(inputString);

        return sb.toString();
    }

}
