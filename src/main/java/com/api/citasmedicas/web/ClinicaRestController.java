package com.api.citasmedicas.web;

import com.api.citasmedicas.domain.Clinica;
import com.api.citasmedicas.service.ClinicaService;
import com.api.citasmedicas.util.MensajeRespuesta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Usuario on 20/11/2020.
 */
@RestController
public class ClinicaRestController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ClinicaService clinicaService;

    @RequestMapping(value = "/clinica/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            List<Clinica> all = this.clinicaService.findAll();

            return new ResponseEntity(all, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/clinica", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getClinicaId(HttpServletRequest request) {
        String id = request.getHeader("x-auth-id");
        String key = request.getHeader("x-auth-key");
        String clinicaId = request.getHeader("x-clinica-id");

        if (id.equalsIgnoreCase("QWE") && key.equalsIgnoreCase("123")) {

            Clinica clinica = this.clinicaService.findByClienteId(clinicaId);

            return new ResponseEntity(clinica, HttpStatus.OK);
        }

        return new ResponseEntity(new MensajeRespuesta(false, "No autorizado"), HttpStatus.BAD_REQUEST);
    }

}
