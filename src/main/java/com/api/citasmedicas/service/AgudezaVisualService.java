package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.AgudezaVisual;

import java.util.List;

/**
 * Created by Usuario on 30/11/2020.
 */
public interface AgudezaVisualService {

    List<AgudezaVisual> findAll();
}
