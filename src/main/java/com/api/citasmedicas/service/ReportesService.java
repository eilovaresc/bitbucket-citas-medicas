package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.ReportResumenControlPacientes;

import java.util.List;

/**
 * Created by Usuario on 17/2/2022.
 */
public interface ReportesService {

    List<ReportResumenControlPacientes> reportResumenControlPacientes(String desde, String hasta, String clinicaId);
}
