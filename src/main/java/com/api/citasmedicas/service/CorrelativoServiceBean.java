package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Correlativo;
import com.api.citasmedicas.domain.CorrelativoPK;
import com.api.citasmedicas.domain.CorrelativoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Usuario on 20/3/2021.
 */
@Service
public class CorrelativoServiceBean implements CorrelativoService {


    @Autowired
    private CorrelativoRepository correlativoRepository;

    @Override
    public Correlativo findById(CorrelativoPK pk) {
        Correlativo correlativo = this.correlativoRepository.findByAnioAndMes(pk.getAnio(), pk.getMes());
        return correlativo;
    }

    @Override
    public void save(Correlativo correlativo) {
        this.correlativoRepository.save(correlativo);
    }
}
