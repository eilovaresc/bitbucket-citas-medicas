package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Paciente;
import com.api.citasmedicas.domain.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 9/11/2020.
 */
@Service("PacienteService")
public class PacienteServiceBean implements PacienteService {

    @Autowired
    private PacienteRepository pacienteRepository;

    @Override
    public void save(Paciente paciente) {
        this.pacienteRepository.save(paciente);
    }

    @Override
    public Paciente findByPacienteId(String pacienteId) {
        return this.pacienteRepository.findByPacienteId(pacienteId);
    }

    @Override
    public List<Paciente> findByNombre(String nombre) {
        return this.pacienteRepository.findByNombreContainingIgnoreCase(nombre);

    }
}
