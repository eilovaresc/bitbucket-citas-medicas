package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Correlativo;
import com.api.citasmedicas.domain.CorrelativoPK;

/**
 * Created by Usuario on 20/3/2021.
 */
public interface CorrelativoService {
    Correlativo findById(CorrelativoPK pk);
    void save(Correlativo correlativo);
}
