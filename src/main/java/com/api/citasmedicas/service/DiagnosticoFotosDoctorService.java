package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.DiagnosticosFotosDoctorDet;
import com.api.citasmedicas.domain.DiagnosticosfotosDoctor;
import com.api.citasmedicas.domain.ViewDiagnosticoFotosDoctor;

import java.util.List;

/**
 * Created by Usuario on 3/7/2021.
 */
public interface DiagnosticoFotosDoctorService {


    DiagnosticosfotosDoctor save(DiagnosticosfotosDoctor diagnosticofoto);


    DiagnosticosFotosDoctorDet saveDetalle(DiagnosticosFotosDoctorDet diagnosticosFotosDet);

    List<ViewDiagnosticoFotosDoctor> findViewByPacienteIdFechaProcesHoraCita(String pacienteId,
                                                                             String fechaProceso,
                                                                             String horaCita);

    ViewDiagnosticoFotosDoctor getbyDetalleId(long id);
}
