package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Especialidad;
import com.api.citasmedicas.domain.EspecialidadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 26/11/2020.
 */
@Service
public class EspecialidadServiceBean implements EspecialidadService {

    @Autowired
    private EspecialidadRepository especialidadRepository;

    @Override
    public List<Especialidad> findAll() {
        return especialidadRepository.findAll();
    }
}
