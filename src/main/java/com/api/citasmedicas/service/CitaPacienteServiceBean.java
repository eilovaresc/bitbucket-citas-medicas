package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.CitaPaciente;
import com.api.citasmedicas.domain.CitaPacienteRepository;
import com.api.citasmedicas.domain.ViewCitaPaciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 13/11/2020.
 */
@Service
public class CitaPacienteServiceBean implements CitaPacienteService {

    @Autowired
    private CitaPacienteRepository citaPacienteRepository;

    @Override
    public void save(CitaPaciente citaPaciente) {
        this.citaPacienteRepository.save(citaPaciente);
    }

    @Override
    public List<ViewCitaPaciente> findByFechaPlanifica(String clinicaId, String fechaPlanificada) {
        //fechaplanificada=YYYYMMDD

        StringBuilder fecha = new StringBuilder(fechaPlanificada);
        fecha.insert(4, '-').insert(7, '-');
        List<ViewCitaPaciente> lista = this.citaPacienteRepository.findByFechaPlanifica(fecha.toString(), clinicaId);
        return lista;
    }

    @Override
    public List<ViewCitaPaciente> findByClinicaIdMedicoIdFechaPlanifica(String clinicaId, String medicoId, String fechaPlanificada) {
        StringBuilder fecha = new StringBuilder(fechaPlanificada);
        fecha.insert(4, '-').insert(7, '-');
        List<ViewCitaPaciente> lista = this.citaPacienteRepository.findByClinicaIdMedicoIdFechaPlanifica(fecha.toString(), clinicaId, medicoId);
        return lista;
    }

    @Override
    public List<ViewCitaPaciente> findByClinicaIdFechaPlanifica(String clinicaId, String fechaPlanificada) {
        StringBuilder fecha = new StringBuilder(fechaPlanificada);
        fecha.insert(4, '-').insert(7, '-');
        List<ViewCitaPaciente> lista = this.citaPacienteRepository.findByClinicaIdFechaPlanifica(fecha.toString(), clinicaId);
        return lista;
    }

    @Override
    public CitaPaciente findByPacienteId(String pacienteId) {
        return this.citaPacienteRepository.findByPacienteId(pacienteId);
    }

    @Override
    public CitaPaciente findByPacienteIdAndFechaPlanificaAndHoraPlanifica(String pacienteId, String fechaPlanifica, String horaPlanifica){
        return this.citaPacienteRepository.findByPacienteIdAndFechaPlanificaAndHoraPlanifica(pacienteId,fechaPlanifica,horaPlanifica);
    }

    @Override
    public void delete(String pacienteId) {
        CitaPaciente citaBorrar = this.citaPacienteRepository.findByPacienteId(pacienteId);
        this.citaPacienteRepository.delete(citaBorrar);
    }
}
