package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Clinica;
import com.api.citasmedicas.domain.ClinicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 13/11/2020.
 */
@Service
public class ClinicaServiceBean implements ClinicaService {

    @Autowired
    private ClinicaRepository clinicaRepository;

    @Override
    public Clinica findByClienteId(String clinicaId) {
        return clinicaRepository.findByClinicaId(clinicaId);
    }

    @Override
    public void save(Clinica clinica) {
        this.clinicaRepository.save(clinica);
    }

    @Override
    public List<Clinica> findAll() {
        return clinicaRepository.findAll();
    }
}
