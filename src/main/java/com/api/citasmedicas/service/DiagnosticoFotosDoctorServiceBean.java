package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.DiagnosticoFotosDoctorDetRepository;
import com.api.citasmedicas.domain.DiagnosticoFotosDoctorRepository;
import com.api.citasmedicas.domain.DiagnosticosFotosDoctorDet;
import com.api.citasmedicas.domain.DiagnosticosfotosDoctor;
import com.api.citasmedicas.domain.ViewDiagnosticoFotosDoctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 3/7/2021.
 */
@Service
public class DiagnosticoFotosDoctorServiceBean implements DiagnosticoFotosDoctorService {


    @Autowired
    private DiagnosticoFotosDoctorRepository diagnosticoFotosRepository;
    @Autowired
    private DiagnosticoFotosDoctorDetRepository diagnosticoFotosDetRepository;

    @Override
    public DiagnosticosfotosDoctor save(DiagnosticosfotosDoctor diagnosticofoto) {
        return this.diagnosticoFotosRepository.save(diagnosticofoto);
    }

    @Override
    public DiagnosticosFotosDoctorDet saveDetalle(DiagnosticosFotosDoctorDet diagnosticosFotosDet) {
        return diagnosticoFotosDetRepository.save(diagnosticosFotosDet);
    }

    @Override
    public List<ViewDiagnosticoFotosDoctor> findViewByPacienteIdFechaProcesHoraCita(String pacienteId, String fechaProceso, String horaCita) {
        return diagnosticoFotosRepository.getPacienteIdAndMdFechaProcesoAndMdHoraCita(pacienteId, fechaProceso, horaCita);
    }

    @Override
    public ViewDiagnosticoFotosDoctor getbyDetalleId(long id) {
        return this.diagnosticoFotosDetRepository.getFotoDoctorDetalleId(id);
    }

}
