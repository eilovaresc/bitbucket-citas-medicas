package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.DiagnosticoFotosDetRepository;
import com.api.citasmedicas.domain.DiagnosticoFotosRepository;
import com.api.citasmedicas.domain.DiagnosticosFotosDet;
import com.api.citasmedicas.domain.Diagnosticosfotos;
import com.api.citasmedicas.domain.ViewDiagnosticoFotos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 3/7/2021.
 */
@Service
public class DiagnosticoFotosServiceBean implements DiagnosticoFotosService {


    @Autowired
    private DiagnosticoFotosRepository diagnosticoFotosRepository;
    @Autowired
    private DiagnosticoFotosDetRepository diagnosticoFotosDetRepository;

    @Override
    public Diagnosticosfotos save(Diagnosticosfotos diagnosticofoto) {
        return this.diagnosticoFotosRepository.save(diagnosticofoto);
    }

    @Override
    public List<Diagnosticosfotos> findAllByPacienteId(String pacienteId) {
        return this.diagnosticoFotosRepository.findByPacienteIdOrderByFechaRegistroDesc(pacienteId);
    }

    @Override
    public List<DiagnosticosFotosDet> findAllByFotoId(Integer fotoid) {
        return this.diagnosticoFotosDetRepository.findAllByFotoId(fotoid);
    }

    @Override
    public List<ViewDiagnosticoFotos> findViewByPacienteId(String pacienteId) {
        return this.diagnosticoFotosDetRepository.findViewByPacienteId(pacienteId);
    }

    @Override
    public DiagnosticosFotosDet saveDetelle(DiagnosticosFotosDet diagnosticosFotosDet) {
        return this.diagnosticoFotosDetRepository.save(diagnosticosFotosDet);
    }

    @Override
    public void deleteFotoDet(DiagnosticosFotosDet foto) {
        this.diagnosticoFotosDetRepository.delete(foto);
    }
}
