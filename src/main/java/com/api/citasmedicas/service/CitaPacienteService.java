package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.CitaPaciente;
import com.api.citasmedicas.domain.ViewCitaPaciente;

import java.util.List;

/**
 * Created by Usuario on 13/11/2020.
 */
public interface CitaPacienteService {

    void save(CitaPaciente citaPaciente);

    List<ViewCitaPaciente> findByFechaPlanifica(String clinicaId, String fechaPlanificada);

    List<ViewCitaPaciente> findByClinicaIdMedicoIdFechaPlanifica(String clinicaId, String medicoId, String fechaPlanificada);


    List<ViewCitaPaciente> findByClinicaIdFechaPlanifica(String clinicaId, String fechaPlanificada);

    CitaPaciente findByPacienteId(String pacienteId);


    CitaPaciente findByPacienteIdAndFechaPlanificaAndHoraPlanifica(String pacienteId, String fechaPlanifica, String horaPlanifica);

    void delete(String pacienteId);

}
