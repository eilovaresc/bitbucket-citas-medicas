package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.DiagnosticoAgudeza;
import com.api.citasmedicas.domain.DiagnosticoAgudezaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 30/11/2020.
 */
@Service
public class DiagnosticoAgudezaServiceBean implements DiagnosticoAgudezaService {
    @Autowired
    private DiagnosticoAgudezaRepository diagnosticoAgudezaRepository;

    @Override
    public List<DiagnosticoAgudeza> findAll() {
        return diagnosticoAgudezaRepository.findAll();
    }
}
