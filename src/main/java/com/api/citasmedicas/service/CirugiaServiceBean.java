package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Cirugia;
import com.api.citasmedicas.domain.CirugiaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 7/12/2020.
 */
@Service
public class CirugiaServiceBean implements CirugiaService {
    @Autowired
    private CirugiaRepository repository;

    @Override
    public List<Cirugia> findAll() {
        return this.repository.findAll();
    }
}
