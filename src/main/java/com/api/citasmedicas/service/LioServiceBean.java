package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Lio;
import com.api.citasmedicas.domain.LioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 7/12/2020.
 */
@Service
public class LioServiceBean implements LioService {

    @Autowired
    private LioRepository repository;

    @Override
    public List<Lio> findAll() {
        return this.repository.findAll();
    }
}
