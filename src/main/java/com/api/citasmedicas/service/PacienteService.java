package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Paciente;

import java.util.List;

/**
 * Created by Usuario on 9/11/2020.
 */
public interface PacienteService {

    void save(Paciente paciente);

    Paciente findByPacienteId(String pacienteId);

    List<Paciente> findByNombre(String nombre);
}
