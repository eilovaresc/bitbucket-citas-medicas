package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.ReportResumenControlPacientes;
import com.api.citasmedicas.domain.ReportesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 17/2/2022.
 */
@Service
public class ReportesServiceBean implements ReportesService {

    @Autowired
    private ReportesRepository repository;

    @Override
    public List<ReportResumenControlPacientes> reportResumenControlPacientes(@Param("desde") String desde, @Param("hasta") String hasta, @Param("clinicaId") String clinicaId) {
        return this.repository.reportResumenControlPacientes(desde, hasta, clinicaId);
    }
}
