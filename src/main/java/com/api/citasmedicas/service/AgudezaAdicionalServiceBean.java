package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.AgudezaAdicional;
import com.api.citasmedicas.domain.AgudezaAdicionalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 30/11/2020.
 */
@Service
public class AgudezaAdicionalServiceBean implements AgudezaAdicionalService {

    @Autowired
    private AgudezaAdicionalRepository repository;

    @Override
    public List<AgudezaAdicional> findAll() {
        return this.repository.findAll();
    }
}
