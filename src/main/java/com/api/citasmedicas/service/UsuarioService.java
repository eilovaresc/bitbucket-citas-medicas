package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Usuario;

import java.util.List;

/**
 * Created by Usuario on 15/11/2020.
 */
public interface UsuarioService {

    List<Usuario> findAll();

    List<Usuario> findByRol(String rol);

    Usuario loginUser(Usuario usuario);
}
