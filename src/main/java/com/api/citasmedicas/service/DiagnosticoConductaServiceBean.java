package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.DiagnosticoConductaRepository;
import com.api.citasmedicas.domain.MdDiagnosticoconducta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 6/12/2020.
 */
@Service
public class DiagnosticoConductaServiceBean implements DiagnosticoConductaService {

    @Autowired
    private DiagnosticoConductaRepository repository;

    @Override
    public List<MdDiagnosticoconducta> findAll() {
        return this.repository.findAll();
    }
}
