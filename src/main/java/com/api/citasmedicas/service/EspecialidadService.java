package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Especialidad;

import java.util.List;

/**
 * Created by Usuario on 26/11/2020.
 */
public interface EspecialidadService {

      List<Especialidad> findAll();
}
