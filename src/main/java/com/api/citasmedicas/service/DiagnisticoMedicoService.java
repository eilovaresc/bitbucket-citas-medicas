package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.DiagnosticoMedico;
import com.api.citasmedicas.domain.ViewDiagnosticoMedico;

import java.util.List;

/**
 * Created by Usuario on 26/11/2020.
 */
public interface DiagnisticoMedicoService {

    void save(DiagnosticoMedico diagnosticoMedico);

    List<ViewDiagnosticoMedico> findAllByPacienteId(String pacienteId);

    List<ViewDiagnosticoMedico> findAllByNombreOrPacienteIdAndClinicaId(String nombre, String pacienteId, String clinicaId);


    List<ViewDiagnosticoMedico> findAllByNombreOrPacienteId(String nombre, String pacienteId);

    DiagnosticoMedico findById(String pacienteId, String fechaProceso, String horaProceso);
}
