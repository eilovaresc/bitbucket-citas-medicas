package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.DiagnosticoAgudeza;

import java.util.List;

/**
 * Created by Usuario on 30/11/2020.
 */
public interface DiagnosticoAgudezaService {

    List<DiagnosticoAgudeza> findAll();
}
