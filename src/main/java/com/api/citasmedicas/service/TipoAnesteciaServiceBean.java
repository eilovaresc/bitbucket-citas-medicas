package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.TipoAnesteciaRepository;
import com.api.citasmedicas.domain.TipoAnestesia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 7/12/2020.
 */
@Service
public class TipoAnesteciaServiceBean implements TipoAnesteciaService {
    @Autowired
    private TipoAnesteciaRepository repository;

    @Override
    public List<TipoAnestesia> findAll() {
        return this.repository.findAll();
    }
}
