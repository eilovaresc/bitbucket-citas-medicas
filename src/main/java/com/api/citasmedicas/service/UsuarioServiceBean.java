package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Usuario;
import com.api.citasmedicas.domain.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 15/11/2020.
 */
@Service("UsuarioService")
public class UsuarioServiceBean implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public List<Usuario> findAll() {
        return this.usuarioRepository.findAll();
    }

    @Override
    public List<Usuario> findByRol(String rol) {
        return this.usuarioRepository.findByRol(rol);
    }

    @Override
    public Usuario loginUser(Usuario usuario) {
        return this.usuarioRepository.loginUsuario(usuario.getUsuarioId(),usuario.getPassword(), usuario.getClinicaId());
    }
}
