package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.AgudezaAdicional;

import java.util.List;

/**
 * Created by Usuario on 30/11/2020.
 */
public interface AgudezaAdicionalService {

    List<AgudezaAdicional> findAll();
}
