package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.DiagnosticoMedico;
import com.api.citasmedicas.domain.DiagnosticoMedicoPK;
import com.api.citasmedicas.domain.DiagnosticoMedicoRepository;
import com.api.citasmedicas.domain.ViewDiagnosticoMedico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Usuario on 26/11/2020.
 */
@Service
public class DiagnisticoMedicoServiceBean implements DiagnisticoMedicoService {

    @Autowired
    private DiagnosticoMedicoRepository diagnosticoMedicoRepository;

    @Override
    public void save(DiagnosticoMedico diagnosticoMedico) {
        this.diagnosticoMedicoRepository.save(diagnosticoMedico);
    }

    @Override
    public List<ViewDiagnosticoMedico> findAllByPacienteId(String pacienteId) {
        return this.diagnosticoMedicoRepository.findAllByPacienteId(pacienteId);
    }

    @Override
    public List<ViewDiagnosticoMedico> findAllByNombreOrPacienteIdAndClinicaId(String nombre, String pacienteId, String clinicaId) {
        return this.diagnosticoMedicoRepository.findAllByNombreOrPacienteIdAndClinicaId(nombre,pacienteId,clinicaId);
    }

    @Override
    public List<ViewDiagnosticoMedico> findAllByNombreOrPacienteId(String nombre, String pacienteId) {
        return this.diagnosticoMedicoRepository.findAllByNombreOrPacienteId(nombre,pacienteId);
    }

    @Override
    public DiagnosticoMedico findById(String pacienteId, String fechaProceso, String horaCita) {
        return this.diagnosticoMedicoRepository.findByMdKeyPacienteIdAndMdFechaProcesoAndMdHoraCita(pacienteId,fechaProceso, horaCita);
    }
}
