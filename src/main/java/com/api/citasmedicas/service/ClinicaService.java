package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Clinica;
import com.api.citasmedicas.domain.ViewCitaPaciente;

import java.util.List;

/**
 * Created by Usuario on 13/11/2020.
 */
public interface ClinicaService {
    Clinica findByClienteId(String clinicaId);

    void save(Clinica clinica);

    List<Clinica> findAll();
}
