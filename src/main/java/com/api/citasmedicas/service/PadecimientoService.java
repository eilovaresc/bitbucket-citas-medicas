package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Padecimiento;

import java.util.List;

/**
 * Created by Usuario on 26/11/2020.
 */
public interface PadecimientoService {

    List<Padecimiento> findAll();
}
