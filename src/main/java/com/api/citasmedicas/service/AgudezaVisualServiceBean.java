package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.AgudezaVisual;
import com.api.citasmedicas.domain.AgudezaVisualRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 30/11/2020.
 */
@Service
public class AgudezaVisualServiceBean implements AgudezaVisualService {

    @Autowired
    private AgudezaVisualRepository visualRepository;

    @Override
    public List<AgudezaVisual> findAll() {
        return this.visualRepository.findAll();
    }
}
