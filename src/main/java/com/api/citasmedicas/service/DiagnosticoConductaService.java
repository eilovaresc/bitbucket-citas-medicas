package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.MdDiagnosticoconducta;

import java.util.List;

/**
 * Created by Usuario on 6/12/2020.
 */
public interface DiagnosticoConductaService {

    List<MdDiagnosticoconducta> findAll();
}
