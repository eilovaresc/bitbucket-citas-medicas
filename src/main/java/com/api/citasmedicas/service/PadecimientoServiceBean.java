package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.Padecimiento;
import com.api.citasmedicas.domain.PadecimientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Usuario on 26/11/2020.
 */
@Service
public class PadecimientoServiceBean implements PadecimientoService {

    @Autowired
    private PadecimientoRepository padecimientoRepository;

    @Override
    public List<Padecimiento> findAll() {
        return padecimientoRepository.findAll();
    }
}
