package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.TipoAnestesia;

import java.util.List;

/**
 * Created by Usuario on 7/12/2020.
 */
public interface TipoAnesteciaService {

    List<TipoAnestesia> findAll();
}
