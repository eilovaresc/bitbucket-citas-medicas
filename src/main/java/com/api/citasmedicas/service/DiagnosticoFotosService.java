package com.api.citasmedicas.service;

import com.api.citasmedicas.domain.DiagnosticosFotosDet;
import com.api.citasmedicas.domain.Diagnosticosfotos;
import com.api.citasmedicas.domain.ViewDiagnosticoFotos;

import java.util.List;

/**
 * Created by Usuario on 3/7/2021.
 */
public interface DiagnosticoFotosService {


    Diagnosticosfotos save(Diagnosticosfotos diagnosticofoto);

    List<Diagnosticosfotos> findAllByPacienteId(String pacienteId);

    List<DiagnosticosFotosDet> findAllByFotoId(Integer fotoid);

    List<ViewDiagnosticoFotos>findViewByPacienteId(String pacienteId);

    DiagnosticosFotosDet saveDetelle(DiagnosticosFotosDet diagnosticosFotosDet);


    void deleteFotoDet(DiagnosticosFotosDet foto);
}
