package com.api.citasmedicas.domain;

/**
 * Created by Usuario on 8/7/2021.
 */
public interface ViewDiagnosticoFotosDoctor {


    int getDetalleId();

    int getFotoId();

    byte[] getFoto();

    String getPacienteId();

    String getFecha();

    String getHora();

    String getNombre();

    String getTipo();

}
