package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 26/11/2020.
 */

@Repository
public interface DiagnosticoMedicoRepository extends JpaRepository<DiagnosticoMedico, String> {

    @Query(value = "select b.md_nombre_paciente as nombrePaciente, " +
            "b.MD_SEXO as sexo, " +
            "a.diagnostico_observaciones_conducta as  conductaObservaciones, " +
            "a.diagnostico_conducta_texto as conductaConducta, " +
            "a.diagnosticoconducta_tratamientos as conductaTratamiento, " +
            "a.md_fecha_proceso as fechaProceso from md_diagnosticomedico a " +
            "left join md_pacientes b on a.md_key_paciente_id=b.KEY_Paciente_ID " +
            "where md_key_paciente_id=:pacienteId order by md_fecha_proceso desc ", nativeQuery = true)
    List<ViewDiagnosticoMedico> findAllByPacienteId(@Param("pacienteId") String pacienteId);

    @Query(value = "select a.md_tipo_consulta as mdTipoConsulta, c.MD_NOMBRE_USUARIO as medico, b.KEY_Paciente_ID as pacienteId, b.md_nombre_paciente as nombrePaciente, " +
            "b.MD_SEXO as sexo, " +
            "a.diagnostico_observaciones_conducta as  conductaObservaciones, " +
            "a.diagnostico_conducta_texto as conductaConducta, " +
            "a.diagnosticoconducta_tratamientos as conductaTratamiento, " +
            "a.md_fecha_proceso as fechaProceso, " +
            "a.md_hora_cita as horaCita from md_diagnosticomedico a " +
            "left join md_pacientes b on a.md_key_paciente_id=b.KEY_Paciente_ID " +
            "left join md_users c on a.key_user_medico_id=c.KEY_USER_ID " +
            "where b.KEY_CLINICA_ID=:clinicaId " +
            "and (md_key_paciente_id=:pacienteId or b.md_nombre_paciente like (%:nombre%)) " +
            "order by md_fecha_proceso desc ", nativeQuery = true)
    List<ViewDiagnosticoMedico> findAllByNombreOrPacienteIdAndClinicaId(@Param("nombre") String nombre,
                                                                        @Param("pacienteId") String pacienteId,
                                                                        @Param("clinicaId") String clinicaId);


    @Query(value = "select d.MD_DESCRIPCION as clinica, a.md_tipo_consulta as mdTipoConsulta, c.MD_NOMBRE_USUARIO as medico, b.KEY_Paciente_ID as pacienteId, b.md_nombre_paciente as nombrePaciente, " +
            "b.MD_SEXO as sexo, " +
            "a.diagnostico_observaciones_conducta as  conductaObservaciones, " +
            "a.diagnostico_conducta_texto as conductaConducta, " +
            "a.diagnosticoconducta_tratamientos as conductaTratamiento, " +
            "a.md_fecha_proceso as fechaProceso, " +
            "a.md_hora_cita as horaCita from md_diagnosticomedico a " +
            "left join md_pacientes b on a.md_key_paciente_id=b.KEY_Paciente_ID " +
            "left join md_users c on a.key_user_medico_id=c.KEY_USER_ID " +
            "left join md_clinicas d on c.KEY_CLINICA_ID=d.KEY_CLINICA_ID " +
            "where (md_key_paciente_id=:pacienteId or b.md_nombre_paciente like (%:nombre%)) " +
            "order by md_fecha_proceso desc ", nativeQuery = true)
    List<ViewDiagnosticoMedico> findAllByNombreOrPacienteId(@Param("nombre") String nombre,
                                                            @Param("pacienteId") String pacienteId);

    DiagnosticoMedico findByMdKeyPacienteIdAndMdFechaProcesoAndMdHoraCita(String paciente, String fechaProceso, String horaCita);
}
