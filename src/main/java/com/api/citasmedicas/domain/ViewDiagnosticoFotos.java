package com.api.citasmedicas.domain;

import java.util.Date;

/**
 * Created by Usuario on 8/7/2021.
 */
public interface ViewDiagnosticoFotos {


    int getDetalleId();

    int getFotoId();

    byte[] getFoto();

    String getOjo();

    String getObsIzquierdo();

    String getObsDerecho();

    String getPacienteId();

    Date getFecha();

    String getTipo();

    String getNombre();

}
