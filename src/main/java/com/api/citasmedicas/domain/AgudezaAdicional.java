package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Usuario on 30/11/2020.
 */
@Entity
@Table(name = "md_agudezaadicional", schema = "db_citasmedicas", catalog = "")
public class AgudezaAdicional {
    private String agudezaAdicionalId;
    private String programaInserta;
    private Timestamp fechaRegistro;

    @Id
    @Column(name = "KEY_AGUDEZAADICIONAL_ID", nullable = false)
    public String getAgudezaAdicionalId() {
        return agudezaAdicionalId;
    }

    public void setAgudezaAdicionalId(String agudezaAdicionalId) {
        this.agudezaAdicionalId = agudezaAdicionalId;
    }

    @Basic
    @Column(name = "AU_programa_inserta", nullable = false)
    public String getProgramaInserta() {
        return programaInserta;
    }

    public void setProgramaInserta(String programaInserta) {
        this.programaInserta = programaInserta;
    }

    @Basic
    @Column(name = "AU_FECHA_REGISTRO", nullable = false)
    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AgudezaAdicional that = (AgudezaAdicional) o;

        if (agudezaAdicionalId != null ? !agudezaAdicionalId.equals(that.agudezaAdicionalId) : that.agudezaAdicionalId != null)
            return false;
        if (programaInserta != null ? !programaInserta.equals(that.programaInserta) : that.programaInserta != null)
            return false;
        return fechaRegistro != null ? fechaRegistro.equals(that.fechaRegistro) : that.fechaRegistro == null;
    }

    @Override
    public int hashCode() {
        int result = agudezaAdicionalId != null ? agudezaAdicionalId.hashCode() : 0;
        result = 31 * result + (programaInserta != null ? programaInserta.hashCode() : 0);
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        return result;
    }
}
