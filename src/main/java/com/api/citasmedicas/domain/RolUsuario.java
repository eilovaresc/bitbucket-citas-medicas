package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Usuario on 15/11/2020.
 */
@Entity
@Table(name = "md_rolesusers")
public class RolUsuario {
    private String keyRolId;
    private String mdDescripcion;
    private String auProgramaInserta;
    private Timestamp auFechaRegistro;

    @Id
    @Column(name = "KEY_ROL_ID", nullable = false)
    public String getKeyRolId() {
        return keyRolId;
    }

    public void setKeyRolId(String keyRolId) {
        this.keyRolId = keyRolId;
    }

    @Basic
    @Column(name = "MD_DESCRIPCION", nullable = false)
    public String getMdDescripcion() {
        return mdDescripcion;
    }

    public void setMdDescripcion(String mdDescripcion) {
        this.mdDescripcion = mdDescripcion;
    }

    @Basic
    @Column(name = "AU_programa_inserta", nullable = false)
    public String getAuProgramaInserta() {
        return auProgramaInserta;
    }

    public void setAuProgramaInserta(String auProgramaInserta) {
        this.auProgramaInserta = auProgramaInserta;
    }

    @Basic
    @Column(name = "AU_FECHA_REGISTRO", nullable = false)
    public Timestamp getAuFechaRegistro() {
        return auFechaRegistro;
    }

    public void setAuFechaRegistro(Timestamp auFechaRegistro) {
        this.auFechaRegistro = auFechaRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RolUsuario that = (RolUsuario) o;

        if (keyRolId != null ? !keyRolId.equals(that.keyRolId) : that.keyRolId != null) return false;
        if (mdDescripcion != null ? !mdDescripcion.equals(that.mdDescripcion) : that.mdDescripcion != null)
            return false;
        if (auProgramaInserta != null ? !auProgramaInserta.equals(that.auProgramaInserta) : that.auProgramaInserta != null)
            return false;
        return auFechaRegistro != null ? auFechaRegistro.equals(that.auFechaRegistro) : that.auFechaRegistro == null;
    }

    @Override
    public int hashCode() {
        int result = keyRolId != null ? keyRolId.hashCode() : 0;
        result = 31 * result + (mdDescripcion != null ? mdDescripcion.hashCode() : 0);
        result = 31 * result + (auProgramaInserta != null ? auProgramaInserta.hashCode() : 0);
        result = 31 * result + (auFechaRegistro != null ? auFechaRegistro.hashCode() : 0);
        return result;
    }
}
