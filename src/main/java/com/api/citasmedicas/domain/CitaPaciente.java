package com.api.citasmedicas.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Created by Usuario on 13/11/2020.
 */
@Entity
@Table(name = "md_citaspacientes")
@IdClass(CitaPacientePK.class)
public class CitaPaciente {
    @Id
    @Column(name = "KEY_Paciente_ID")
    private String pacienteId;
    @Column(name = "KEY_CLINICA_ID")
    private String clinicaId;
    @Column(name = "KEY_USER_MEDICO_ID")
    private String medicoId;
    @Column(name = "KEY_ESPECIALIDAD_ID")
    private String especialidad;
    @Column(name = "KEY_PADECIMIENTO_ID")
    private String padecimiento;
    @Id
    @Column(name = "MD_FECHA_PLANIFICADA")
    private String fechaPlanifica;
    @Id
    @Column(name = "MD_HORA_PLANIFICADA")
    private String horaPlanifica;
    @Column(name = "MD_ESTADO_CITA")
    private String estado;
    @Column(name = "MD_TIPO_CONSULTA")
    private String tipoConsulta;
    @Column(name = "MD_GLICEMIA")
    private String glicemia;
    @Column(name = "MD_FACTURA")
    private String factura;

    public String getGlicemia() {
        return glicemia;
    }

    public void setGlicemia(String glicemia) {
        this.glicemia = glicemia;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getTipoConsulta() {
        return tipoConsulta;
    }

    public void setTipoConsulta(String tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }

    public String getPacienteId() {
        return pacienteId;
    }

    public void setPacienteId(String pacienteId) {
        this.pacienteId = pacienteId;
    }

    public String getClinicaId() {
        return clinicaId;
    }

    public void setClinicaId(String clinicaId) {
        this.clinicaId = clinicaId;
    }

    public String getMedicoId() {
        return medicoId;
    }

    public void setMedicoId(String medicoId) {
        this.medicoId = medicoId;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getPadecimiento() {
        return padecimiento;
    }

    public void setPadecimiento(String padecimiento) {
        this.padecimiento = padecimiento;
    }

    public String getFechaPlanifica() {
        return fechaPlanifica;
    }

    public void setFechaPlanifica(String fechaPlanifica) {
        this.fechaPlanifica = fechaPlanifica;
    }

    public String getHoraPlanifica() {
        return horaPlanifica;
    }

    public void setHoraPlanifica(String horaPlanifica) {
        this.horaPlanifica = horaPlanifica;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
