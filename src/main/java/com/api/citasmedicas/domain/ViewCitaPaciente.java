package com.api.citasmedicas.domain;

/**
 * Created by Usuario on 15/11/2020.
 */
public interface ViewCitaPaciente {


    String getTipoConsulta();

    String getPacienteId();

    String getNombrePaciente();

    String getClinicaId();

    String getMedicoId();

    String getMedicoNombre();

    String getEspecialidad();

    String getPadecimiento();

    String getFechaPlanifica();

    String getHoraPlanifica();

    String getEstado();

    String getSexo();

    Integer getEdad();

    String getClinicaNombre();

     String getGlicemia();

     String getFactura();


}
