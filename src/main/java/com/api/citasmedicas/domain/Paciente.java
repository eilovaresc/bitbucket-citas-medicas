package com.api.citasmedicas.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Usuario on 9/11/2020.
 */
@Entity
@Table(name = "md_pacientes")
public class Paciente {

    @Id
    @Column(name = "KEY_Paciente_ID")
    private String pacienteId;

    @Column(name = "KEY_DPI_ID")
    private String keyDpiId;

    @Column(name = "md_nombre_paciente")
    private String nombre;

    @Column(name = "MD_FECHA_NACIMIENTO")
    private Date fechaNacimiento;

    @Column(name = "MD_TIPO_SANGRE")
    private String tipoSangre;

    @Column(name = "MD_ESTADO_CIVIL")
    private String estadoCivil;

    @Column(name = "MD_SEXO")
    private String sexo;

    @Column(name = "MD_DIRECCION")
    private String direccion;

    @Column(name = "MD_TELEFONO")
    private String telefono;

    @Column(name = "KEY_Departamento_ID")
    private String departamentoId;

    @Column(name = "KEY_USER_ID")
    private String usuarioId;

    @Column(name = "AU_programa_inserta")
    private String programaInserta;

    @Column(name = "AU_FECHA_REGISTRO")
    private Date fechaRegistro;

    @Column(name = "KEY_CLINICA_ID")
    private String clinicaId;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClinicaId() {
        return clinicaId;
    }

    public void setClinicaId(String clinicaId) {
        this.clinicaId = clinicaId;
    }

    public String getPacienteId() {
        return pacienteId;
    }

    public void setPacienteId(String pacienteId) {
        this.pacienteId = pacienteId;
    }

    public String getKeyDpiId() {
        return keyDpiId;
    }

    public void setKeyDpiId(String keyDpiId) {
        this.keyDpiId = keyDpiId;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTipoSangre() {
        return tipoSangre;
    }

    public void setTipoSangre(String tipoSangre) {
        this.tipoSangre = tipoSangre;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDepartamentoId() {
        return departamentoId;
    }

    public void setDepartamentoId(String departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getProgramaInserta() {
        return programaInserta;
    }

    public void setProgramaInserta(String programaInserta) {
        this.programaInserta = programaInserta;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}
