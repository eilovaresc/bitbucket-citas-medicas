package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Usuario on 7/12/2020.
 */
@Entity
@Table(name = "md_lio", schema = "db_citasmedicas")
public class Lio {
    private String lioId;
    private String programaInserta;
    private Timestamp fechaRegistro;

    @Id
    @Column(name = "KEY_LIO_ID", nullable = false)
    public String getLioId() {
        return lioId;
    }

    public void setLioId(String lioId) {
        this.lioId = lioId;
    }

    @Basic
    @Column(name = "AU_programa_inserta", nullable = false)
    public String getProgramaInserta() {
        return programaInserta;
    }

    public void setProgramaInserta(String programaInserta) {
        this.programaInserta = programaInserta;
    }

    @Basic
    @Column(name = "AU_FECHA_REGISTRO", nullable = false)
    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lio lio = (Lio) o;

        if (lioId != null ? !lioId.equals(lio.lioId) : lio.lioId != null) return false;
        if (programaInserta != null ? !programaInserta.equals(lio.programaInserta) : lio.programaInserta != null)
            return false;
        if (fechaRegistro != null ? !fechaRegistro.equals(lio.fechaRegistro) : lio.fechaRegistro != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = lioId != null ? lioId.hashCode() : 0;
        result = 31 * result + (programaInserta != null ? programaInserta.hashCode() : 0);
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        return result;
    }
}
