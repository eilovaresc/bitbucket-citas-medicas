package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 9/11/2020.
 */
@Repository
public interface PacienteRepository extends JpaRepository<Paciente, String> {

    Paciente findByPacienteId(String pacienteId);

    List<Paciente> findByNombreContainingIgnoreCase(String nombre);
}
