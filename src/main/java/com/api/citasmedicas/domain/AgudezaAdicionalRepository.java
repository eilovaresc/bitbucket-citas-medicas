package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 30/11/2020.
 */
@Repository
public interface AgudezaAdicionalRepository extends JpaRepository<AgudezaAdicional, String> {

    List<AgudezaAdicional> findAll();
}
