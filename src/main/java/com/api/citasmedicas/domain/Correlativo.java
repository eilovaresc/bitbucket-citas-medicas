package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Created by Usuario on 20/3/2021.
 */
@Entity
@Table(name = "md_correlativos", schema = "db_citasmedicas", catalog = "")
@IdClass(CorrelativoPK.class)
public class Correlativo {
    private int anio;
    private int mes;
    private Integer correlativo;

    @Id
    @Column(name = "anio", nullable = false)
    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    @Id
    @Column(name = "mes", nullable = false)
    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    @Basic
    @Column(name = "correlativo", nullable = true)
    public Integer getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(Integer correlativo) {
        this.correlativo = correlativo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Correlativo that = (Correlativo) o;

        if (anio != that.anio) return false;
        if (mes != that.mes) return false;
        if (correlativo != null ? !correlativo.equals(that.correlativo) : that.correlativo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = anio;
        result = 31 * result + mes;
        result = 31 * result + (correlativo != null ? correlativo.hashCode() : 0);
        return result;
    }
}
