package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 17/2/2022.
 */
@Repository
public interface ReportesRepository extends JpaRepository<TipoAnestesia, String> {


    @Query(value = "select KEY_CLINICA_ID clinicaId, MD_NOMBRE_USUARIO medicoId, MD_TIPO_CONSULTA tipoConsulta, sum(FINALIZADOS) finalizadas, SUM(ACTIVOS) activas, SUM(TOTALES) totales, glicemiaSi, glicemiaNo FROM (SELECT " +
            "mc.KEY_CLINICA_ID, " +
            "us.MD_NOMBRE_USUARIO, " +
            "MD_TIPO_CONSULTA,  " +
            "case when mc.MD_ESTADO_CITA='F' then count(1) ELSE 0 END as FINALIZADOS, " +
            "case when mc.MD_ESTADO_CITA='A' then count(1) ELSE 0 END as ACTIVOS, " +
            "count(1) TOTALES, " +
            "case when mc.MD_glicemia ='S' then count(1) else  0 end as glicemiaSi, " +
            "case when (mc.MD_glicemia <>'S') then count(1) else  0 end as glicemiaNo, " +
            "mc.md_factura " +
            "from db_citasmedicas.md_citaspacientes mc " +
            "left join db_citasmedicas.md_users us on mc.KEY_USER_MEDICO_ID=us.KEY_USER_ID " +
            "where mc.MD_FECHA_PLANIFICADA between :desde and :hasta  and MD_TIPO_CONSULTA is not null and mc.KEY_USER_MEDICO_ID is not null  " +
            "group by KEY_CLINICA_ID, MD_NOMBRE_USUARIO, MD_TIPO_CONSULTA, mc.MD_ESTADO_CITA, mc.MD_glicemia) AS TABLA " +
            "WHERE CASE WHEN :clinicaId IS NULL THEN '' ELSE KEY_CLINICA_ID END=CASE WHEN :clinicaId IS NULL THEN '' ELSE :clinicaId END " +
            "GROUP BY KEY_CLINICA_ID, MD_NOMBRE_USUARIO, MD_TIPO_CONSULTA,glicemiaSi,glicemiaNo " +
            "order by clinicaId, medicoId, tipoConsulta;", nativeQuery = true)
    List<ReportResumenControlPacientes> reportResumenControlPacientes(@Param("desde") String desde,
                                                                      @Param("hasta") String hasta,
                                                                      @Param("clinicaId") String clinicaId);
}
