package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 3/7/2021.
 */

@Repository
public interface DiagnosticoFotosDoctorRepository extends JpaRepository<DiagnosticosfotosDoctor, Long> {

    DiagnosticosfotosDoctor save(DiagnosticosfotosDoctor s);


    @Query(value = "SELECT detalle_id as detalleId, a.foto_id as fotoId, \n" +
            "paciente_id as pacienteId, \n" +
            "a.tipo as tipo, \n" +
            "a.nombre_imagen as nombre \n" +
            "FROM db_citasfotosdoctor.md_diagnosticosfotosdet a  \n" +
            "left join db_citasfotosdoctor.md_diagnosticosfotos b on a.foto_id=b.foto_id   \n" +
            "where b.paciente_id=:pacienteId and b.md_fecha_proceso=:fechaProceso and b.md_hora_cita=:horaCita ", nativeQuery = true)
    List<ViewDiagnosticoFotosDoctor> getPacienteIdAndMdFechaProcesoAndMdHoraCita(@Param("pacienteId") String pacienteId,
                                                                                 @Param("fechaProceso") String fechaProceso,
                                                                                 @Param("horaCita") String horaCita);
}
