package com.api.citasmedicas.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Usuario on 1/12/2020.
 */
public class DiagnosticoMedicoPK implements Serializable {
    private String mdKeyPacienteId;
    private String mdFechaProceso;
    private String mdHoraCita;

    @Column(name = "md_key_paciente_id", nullable = false)
    @Id
    public String getMdKeyPacienteId() {
        return mdKeyPacienteId;
    }

    public void setMdKeyPacienteId(String mdKeyPacienteId) {
        this.mdKeyPacienteId = mdKeyPacienteId;
    }

    @Column(name = "md_fecha_proceso", nullable = false)
    @Id
    public String getMdFechaProceso() {
        return mdFechaProceso;
    }

    public void setMdFechaProceso(String mdFechaProceso) {
        this.mdFechaProceso = mdFechaProceso;
    }

    @Id
    @Column(name = "md_hora_cita", nullable = false)
    public String getMdHoraCita() {
        return mdHoraCita;
    }

    public void setMdHoraCita(String mdHoraCita) {
        this.mdHoraCita = mdHoraCita;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DiagnosticoMedicoPK that = (DiagnosticoMedicoPK) o;

        if (mdKeyPacienteId != null ? !mdKeyPacienteId.equals(that.mdKeyPacienteId) : that.mdKeyPacienteId != null)
            return false;
        if (mdFechaProceso != null ? !mdFechaProceso.equals(that.mdFechaProceso) : that.mdFechaProceso != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = mdKeyPacienteId != null ? mdKeyPacienteId.hashCode() : 0;
        result = 31 * result + (mdFechaProceso != null ? mdFechaProceso.hashCode() : 0);
        return result;
    }
}
