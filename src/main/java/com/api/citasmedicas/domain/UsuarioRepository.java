package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 15/11/2020.
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, String> {

    List<Usuario> findByRol(String rol);

    @Query("SELECT u FROM Usuario u where clinicaId=:clinicaId and usuarioId=:usuarioId and password=md5(:password)")
    Usuario loginUsuario(@Param("usuarioId") String usuarioId,
                         @Param("password") String password,
                         @Param("clinicaId") String clinicaId);

}
