package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 6/7/2021.
 */
@Repository
public interface DiagnosticoFotosDetRepository extends JpaRepository<DiagnosticosFotosDet, Long> {

    DiagnosticosFotosDet save(DiagnosticosFotosDet s);

    List<DiagnosticosFotosDet> findAllByFotoId(Integer fotoId);


    @Query(value = "SELECT detalle_id as detalleId,a.foto_id as fotoId,foto,ojo,a.fecha_registro as fecha, \n" +
            "paciente_id as pacienteId, \n" +
            "observaciones_izquierdo as obsIzquierdo,\n " +
            "observaciones_derecho as obsDerecho,\n " +
            "a.tipo as tipo, " +
            "a.nombre_imagen as nombre " +
            "FROM db_citasfotos.md_diagnosticosfotosdet a " +
            "left join db_citasfotos.md_diagnosticosfotos b on a.foto_id=b.foto_id  " +
            "where b.paciente_id=:pacienteId order by a.fecha_registro desc ", nativeQuery = true)
    List<ViewDiagnosticoFotos> findViewByPacienteId(@Param("pacienteId") String pacienteId);

}
