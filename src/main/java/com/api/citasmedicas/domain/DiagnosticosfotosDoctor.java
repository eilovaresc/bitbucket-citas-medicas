package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

/**
 * Created by Usuario on 2/7/2021.
 */
@Entity
@Table(name = "md_diagnosticosfotos", schema = "db_citasfotosdoctor", catalog = "db_citasfotosdoctor")
public class DiagnosticosfotosDoctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "foto_id", nullable = false)
    private long fotoId;

    @Basic
    @Column(name = "paciente_id", nullable = false, length = 15)
    private String pacienteId;

    @Basic
    @Column(name = "md_fecha_proceso", nullable = false, length = 10)
    private String mdFechaProceso;


    @Basic
    @Column(name = "md_hora_cita", nullable = false, length = 10)
    private String mdHoraCita;


    @Basic
    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    public String getMdFechaProceso() {
        return mdFechaProceso;
    }

    public void setMdFechaProceso(String mdFechaProceso) {
        this.mdFechaProceso = mdFechaProceso;
    }

    public String getMdHoraCita() {
        return mdHoraCita;
    }

    public void setMdHoraCita(String mdHoraCita) {
        this.mdHoraCita = mdHoraCita;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public long getFotoId() {
        return fotoId;
    }

    public void setFotoId(long fotoId) {
        this.fotoId = fotoId;
    }

    public String getPacienteId() {
        return pacienteId;
    }

    public void setPacienteId(String pacienteId) {
        this.pacienteId = pacienteId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DiagnosticosfotosDoctor)) return false;
        DiagnosticosfotosDoctor that = (DiagnosticosfotosDoctor) o;
        return getFotoId() == that.getFotoId() &&
                Objects.equals(getPacienteId(), that.getPacienteId()) &&
                Objects.equals(getMdFechaProceso(), that.getMdFechaProceso()) &&
                Objects.equals(getMdHoraCita(), that.getMdHoraCita()) &&
                Objects.equals(getUsuarioRegistro(), that.getUsuarioRegistro());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getFotoId(), getPacienteId(), getMdFechaProceso(), getMdHoraCita(), getUsuarioRegistro());
    }
}
