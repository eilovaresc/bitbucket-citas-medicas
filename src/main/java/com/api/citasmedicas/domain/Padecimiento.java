package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Usuario on 26/11/2020.
 */
@Entity
@Table(name = "md_padecimientos", schema = "db_citasmedicas")
public class Padecimiento {
    private String padecimientoId;
    private String auProgramaInserta;
    private Timestamp auFechaRegistro;

    @Id
    @Column(name = "KEY_PADECIMIENTO_ID", nullable = false)
    public String getPadecimientoId() {
        return padecimientoId;
    }

    public void setPadecimientoId(String padecimientoId) {
        this.padecimientoId = padecimientoId;
    }

    @Basic
    @Column(name = "AU_programa_inserta", nullable = false)
    public String getAuProgramaInserta() {
        return auProgramaInserta;
    }

    public void setAuProgramaInserta(String auProgramaInserta) {
        this.auProgramaInserta = auProgramaInserta;
    }

    @Basic
    @Column(name = "AU_FECHA_REGISTRO", nullable = false)
    public Timestamp getAuFechaRegistro() {
        return auFechaRegistro;
    }

    public void setAuFechaRegistro(Timestamp auFechaRegistro) {
        this.auFechaRegistro = auFechaRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Padecimiento that = (Padecimiento) o;

        if (padecimientoId != null ? !padecimientoId.equals(that.padecimientoId) : that.padecimientoId != null)
            return false;
        if (auProgramaInserta != null ? !auProgramaInserta.equals(that.auProgramaInserta) : that.auProgramaInserta != null)
            return false;
        return auFechaRegistro != null ? auFechaRegistro.equals(that.auFechaRegistro) : that.auFechaRegistro == null;
    }

    @Override
    public int hashCode() {
        int result = padecimientoId != null ? padecimientoId.hashCode() : 0;
        result = 31 * result + (auProgramaInserta != null ? auProgramaInserta.hashCode() : 0);
        result = 31 * result + (auFechaRegistro != null ? auFechaRegistro.hashCode() : 0);
        return result;
    }
}
