package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Usuario on 20/3/2021.
 */
@Repository
public interface CorrelativoRepository extends JpaRepository<Correlativo, CorrelativoPK> {

    Correlativo findByAnioAndMes(int anio, int mes);
}
