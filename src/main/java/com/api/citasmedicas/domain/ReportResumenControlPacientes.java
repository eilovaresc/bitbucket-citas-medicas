package com.api.citasmedicas.domain;

/**
 * Created by Usuario on 17/2/2022.
 */
public interface ReportResumenControlPacientes {

    String getClinicaId();
    String getTipoConsulta();
    String getFinalizadas();
    String getActivas();
    String getGlicemiaSi();
    String getGlicemiaNo();
    String getTotales();
    String getMedicoId();
}
