package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Usuario on 6/7/2021.
 */
@Entity
@Table(name = "md_diagnosticosfotosdet", schema = "db_citasfotosdoctor", catalog = "db_citasfotosdoctor")
public class DiagnosticosFotosDoctorDet {
    private int detalleId;
    private int fotoId;
    private byte[] foto;
    private String nombreImagen;
    @Basic
    @Column(name = "tipo", nullable = false)
    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "nombre_imagen", nullable = true)
    public String getNombreImagen() {
        return nombreImagen;
    }

    public void setNombreImagen(String nombreImagen) {
        this.nombreImagen = nombreImagen;
    }


    @Id
    @Column(name = "detalle_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getDetalleId() {
        return detalleId;
    }

    public void setDetalleId(int detalleId) {
        this.detalleId = detalleId;
    }

    @Basic
    @Column(name = "foto_id", nullable = false)
    public int getFotoId() {
        return fotoId;
    }

    public void setFotoId(int fotoId) {
        this.fotoId = fotoId;
    }

    @Basic
    @Column(name = "foto", nullable = true)
    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DiagnosticosFotosDoctorDet)) return false;
        DiagnosticosFotosDoctorDet that = (DiagnosticosFotosDoctorDet) o;
        return getDetalleId() == that.getDetalleId() &&
                getFotoId() == that.getFotoId() &&
                Arrays.equals(getFoto(), that.getFoto()) &&
                Objects.equals(getNombreImagen(), that.getNombreImagen());
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(getDetalleId(), getFotoId(), getNombreImagen());
        result = 31 * result + Arrays.hashCode(getFoto());
        return result;
    }
}
