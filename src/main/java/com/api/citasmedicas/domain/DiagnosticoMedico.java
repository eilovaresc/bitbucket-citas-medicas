package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Created by Usuario on 1/12/2020.
 */
@Entity
@Table(name = "md_diagnosticomedico", schema = "db_citasmedicas", catalog = "")
@IdClass(DiagnosticoMedicoPK.class)
public class DiagnosticoMedico {
    private String mdKeyPacienteId;
    private String mdFechaProceso;
    private String mdHoraCita;
    private String mdHoraProceso;
    private String mdHoraProcesoFin;
    private String medicoId;
    private String mdEstadoDiagnostico;
    private String mdMotivosDeConsulta;
    private String mdAntecedentesPersonales;
    private String mdAntecedentesFamiliares;
    private String mdRevisionConsulta;
    private String mdCirugiasPrevias;
    private String odKeyAgudezavisualId;
    private String odCalificacionAgudezavisualId;
    private String odKeyAgudezavisualConCorreccionId;
    private String odCalificacionConCorreccionId;
    private String odKeyAgudezavisualConNuevacorreccionId;
    private String odCalificacionConNuevacorreccionId;
    private String odSinciclopedia;
    private String odConciclopedia;
    private String odFinal;
    private String odKeyAgudezaadicionalId;
    private String odKeratrometia;
    private String odLensometria;
    private String osKeyAgudezavisualId;
    private String osCalificacionAgudezavisualId;
    private String osKeyAgudezavisualConCorreccionId;
    private String osCalificacionConCorreccionId;
    private String osKeyAgudezavisualConNuevacorreccionId;
    private String osCalificacionConNuevacorreccionId;
    private String osSinciclopedia;
    private String osConciclopedia;
    private String osFinal;
    private String osKeyAgudezaadicionalId;
    private String osKeratrometia;
    private String osLensometria;
    private String decercaKeyAgudezavisualId;
    private String decercaCalificacionAgudezavisualId;
    private String decercaKeyAgudezavisualConCorreccionId;
    private String decercaCalificacionConCorreccionId;
    private String decercaKeyAgudezavisualConNuevacorreccionId;
    private String decercaCalificacionConNuevacorreccionId;
    private String decercaSinciclopedia;
    private String decercaConciclopedia;
    private String diagnostico1KeyDiagnosticoagudezaOd;
    private String diagnostico2KeyDiagnosticoagudezaOi;
    private String diagnostico3KeyDiagnosticoagudezaPresbicia;
    private String observacionesAgudezavisual;
    private String odMotilidad1;
    private String odMotilidad2;
    private String odMotilidad3;
    private String odViasLagrimales;
    private String odParpadosPestanas;
    private String odConjuntiva;
    private String odEsclera;
    private String odCornea;
    private String odCamaraAnterior;
    private String odIris;
    private String odPupila;
    private String odTio;
    private String odGonioscopia;
    private String odCristalino;
    private String odVitreo;
    private String odRetina;
    private String odNervioOptico;
    private String odOrbita;
    private String odObservaciones;
    private String osMotilidad1;
    private String osMotilidad2;
    private String osMotilidad3;
    private String osViasLagrimales;
    private String osParpadosPestanas;
    private String osConjuntiva;
    private String osEsclera;
    private String osCornea;
    private String osCamaraAnterior;
    private String osIris;
    private String osPupila;
    private String osTio;
    private String osGonioscopia;
    private String osCristalino;
    private String osVitreo;
    private String osRetina;
    private String osNervioOptico;
    private String osOrbita;
    private String osObservaciones;
    private String diagnostico1KeyDiagnosticoconductaId;
    private String diagnosticoconductaDiagnostico1KeyOrganoId;
    private String diagnostico2KeyDiagnosticoconductaId;
    private String diagnosticoconductaDiagnostico2KeyOrganoId;
    private String diagnostico3KeyDiagnosticoconductaId;
    private String diagnosticoconductaDiagnostico3KeyOrganoId;
    private String diagnostico4KeyDiagnosticoconductaId;
    private String diagnosticoconductaDiagnostico4KeyOrganoId;
    private String diagnostico5KeyDiagnosticoconductaId;
    private String diagnosticoconductaDiagnostico5KeyOrganoId;
    private String diagnosticoObservacionesConducta;
    private String diagnosticoConductaTexto;
    private String mdEstadoOperaciones;
    private String mdQuirofano;
    private String mdCirujano;
    private String keyTipoanestesiaId;
    private String mdAnesteciologo;
    private String keyCirugiaId;
    private String mdAyudante;
    private String mdHorainicio;
    private String mdHorafinalizacion;
    private String keyLioId;
    private String salaOperacionesObservaciones;
    private String salaOperacionesMotivo;
    private String salaOperacionesDiagnostico;
    private String esferaOd;
    private String cilindroOd;
    private String ejeOd;
    private String prismaOd;
    private String adicionOd;
    private String dipOd;
    private String esferaOi;
    private String cilindroOi;
    private String ejeOi;
    private String prismaOi;
    private String adicionOi;
    private String dipOi;
    private String odPhKeyAgudezavisualId;
    private String odPhCalificacionAgudezavisualId;
    private String osPhKeyAgudezavisualId;
    private String osPhCalificacionAgudezavisualId;
    private String decercaPhKeyAgudezavisualId;
    private String decercaPhCalificacionAgudezavisualId;
    private String diagnosticoconductaTratamientos;
    private String salaOperacionesComplicaciones;
    private String alturaOd;
    private String alturaOi;
    private String osGonioscopiaAbajo;
    private String osGonioscopiaIzquierda;
    private String osGonioscopiaDerecha;
    private String odGonioscopiaAbajo;
    private String odGonioscopiaIzquierda;
    private String odGonioscopiaDerecha;
    private String mdTipoConsulta;
    private String odPio;
    private String osPio;
    private String decercaPio;
    private String odEmetropia;
    private String osEmetropia;
    private String decercaEmetropia;

    @Id
    @Column(name = "md_key_paciente_id", nullable = false)
    public String getMdKeyPacienteId() {
        return mdKeyPacienteId;
    }

    public void setMdKeyPacienteId(String mdKeyPacienteId) {
        this.mdKeyPacienteId = mdKeyPacienteId;
    }

    @Id
    @Column(name = "md_fecha_proceso", nullable = false)
    public String getMdFechaProceso() {
        return mdFechaProceso;
    }

    public void setMdFechaProceso(String mdFechaProceso) {
        this.mdFechaProceso = mdFechaProceso;
    }

    @Id
    @Basic
    @Column(name = "md_hora_cita", nullable = false)
    public String getMdHoraCita() {
        return mdHoraCita;
    }

    public void setMdHoraCita(String mdHoraCita) {
        this.mdHoraCita = mdHoraCita;
    }


    @Basic
    @Column(name = "md_tipo_consulta", nullable = true)
    public String getMdTipoConsulta() {
        return mdTipoConsulta;
    }

    public void setMdTipoConsulta(String mdTipoConsulta) {
        this.mdTipoConsulta = mdTipoConsulta;
    }

    @Basic
    @Column(name = "os_gonioscopia_abajo", nullable = true)
    public String getOsGonioscopiaAbajo() {
        return osGonioscopiaAbajo;
    }

    public void setOsGonioscopiaAbajo(String osGonioscopiaAbajo) {
        this.osGonioscopiaAbajo = osGonioscopiaAbajo;
    }

    @Basic
    @Column(name = "os_gonioscopia_izquierda", nullable = true)
    public String getOsGonioscopiaIzquierda() {
        return osGonioscopiaIzquierda;
    }

    public void setOsGonioscopiaIzquierda(String osGonioscopiaIzquierda) {
        this.osGonioscopiaIzquierda = osGonioscopiaIzquierda;
    }

    @Basic
    @Column(name = "os_gonioscopia_derecha", nullable = true)
    public String getOsGonioscopiaDerecha() {
        return osGonioscopiaDerecha;
    }

    public void setOsGonioscopiaDerecha(String osGonioscopiaDerecha) {
        this.osGonioscopiaDerecha = osGonioscopiaDerecha;
    }

    @Basic
    @Column(name = "od_gonioscopia_abajo", nullable = true)
    public String getOdGonioscopiaAbajo() {
        return odGonioscopiaAbajo;
    }

    public void setOdGonioscopiaAbajo(String odGonioscopiaAbajo) {
        this.odGonioscopiaAbajo = odGonioscopiaAbajo;
    }

    @Basic
    @Column(name = "od_gonioscopia_izquierda", nullable = true)
    public String getOdGonioscopiaIzquierda() {
        return odGonioscopiaIzquierda;
    }

    public void setOdGonioscopiaIzquierda(String odGonioscopiaIzquierda) {
        this.odGonioscopiaIzquierda = odGonioscopiaIzquierda;
    }

    @Basic
    @Column(name = "od_gonioscopia_derecha", nullable = true)
    public String getOdGonioscopiaDerecha() {
        return odGonioscopiaDerecha;
    }

    public void setOdGonioscopiaDerecha(String odGonioscopiaDerecha) {
        this.odGonioscopiaDerecha = odGonioscopiaDerecha;
    }

    @Basic
    @Column(name = "key_user_medico_id", nullable = true)
    public String getMedicoId() {
        return medicoId;
    }

    public void setMedicoId(String medicoId) {
        this.medicoId = medicoId;
    }


    @Basic
    @Column(name = "md_hora_proceso", nullable = true)
    public String getMdHoraProceso() {
        return mdHoraProceso;
    }

    public void setMdHoraProceso(String mdHoraProceso) {
        this.mdHoraProceso = mdHoraProceso;
    }

    @Basic
    @Column(name = "md_hora_proceso_fin", nullable = true)
    public String getMdHoraProcesoFin() {
        return mdHoraProcesoFin;
    }

    public void setMdHoraProcesoFin(String mdHoraProcesoFin) {
        this.mdHoraProcesoFin = mdHoraProcesoFin;
    }

    @Basic
    @Column(name = "md_estado_diagnostico", nullable = true)
    public String getMdEstadoDiagnostico() {
        return mdEstadoDiagnostico;
    }

    public void setMdEstadoDiagnostico(String mdEstadoDiagnostico) {
        this.mdEstadoDiagnostico = mdEstadoDiagnostico;
    }

    @Basic
    @Column(name = "md_motivos_de_consulta", nullable = true)
    public String getMdMotivosDeConsulta() {
        return mdMotivosDeConsulta;
    }

    public void setMdMotivosDeConsulta(String mdMotivosDeConsulta) {
        this.mdMotivosDeConsulta = mdMotivosDeConsulta;
    }

    @Basic
    @Column(name = "md_antecedentes_personales", nullable = true)
    public String getMdAntecedentesPersonales() {
        return mdAntecedentesPersonales;
    }

    public void setMdAntecedentesPersonales(String mdAntecedentesPersonales) {
        this.mdAntecedentesPersonales = mdAntecedentesPersonales;
    }

    @Basic
    @Column(name = "md_antecedentes_familiares", nullable = true)
    public String getMdAntecedentesFamiliares() {
        return mdAntecedentesFamiliares;
    }

    public void setMdAntecedentesFamiliares(String mdAntecedentesFamiliares) {
        this.mdAntecedentesFamiliares = mdAntecedentesFamiliares;
    }

    @Basic
    @Column(name = "md_revision_consulta", nullable = true)
    public String getMdRevisionConsulta() {
        return mdRevisionConsulta;
    }

    public void setMdRevisionConsulta(String mdRevisionConsulta) {
        this.mdRevisionConsulta = mdRevisionConsulta;
    }

    @Basic
    @Column(name = "md_cirugias_previas", nullable = true)
    public String getMdCirugiasPrevias() {
        return mdCirugiasPrevias;
    }

    public void setMdCirugiasPrevias(String mdCirugiasPrevias) {
        this.mdCirugiasPrevias = mdCirugiasPrevias;
    }

    @Basic
    @Column(name = "od_key_agudezavisual_id", nullable = true)
    public String getOdKeyAgudezavisualId() {
        return odKeyAgudezavisualId;
    }

    public void setOdKeyAgudezavisualId(String odKeyAgudezavisualId) {
        this.odKeyAgudezavisualId = odKeyAgudezavisualId;
    }

    @Basic
    @Column(name = "od_calificacion_agudezavisual_id", nullable = true)
    public String getOdCalificacionAgudezavisualId() {
        return odCalificacionAgudezavisualId;
    }

    public void setOdCalificacionAgudezavisualId(String odCalificacionAgudezavisualId) {
        this.odCalificacionAgudezavisualId = odCalificacionAgudezavisualId;
    }

    @Basic
    @Column(name = "od_key_agudezavisual_con_correccion_id", nullable = true)
    public String getOdKeyAgudezavisualConCorreccionId() {
        return odKeyAgudezavisualConCorreccionId;
    }

    public void setOdKeyAgudezavisualConCorreccionId(String odKeyAgudezavisualConCorreccionId) {
        this.odKeyAgudezavisualConCorreccionId = odKeyAgudezavisualConCorreccionId;
    }

    @Basic
    @Column(name = "od_calificacion_con_correccion_id", nullable = true)
    public String getOdCalificacionConCorreccionId() {
        return odCalificacionConCorreccionId;
    }

    public void setOdCalificacionConCorreccionId(String odCalificacionConCorreccionId) {
        this.odCalificacionConCorreccionId = odCalificacionConCorreccionId;
    }

    @Basic
    @Column(name = "od_key_agudezavisual_con_nuevacorreccion_id", nullable = true)
    public String getOdKeyAgudezavisualConNuevacorreccionId() {
        return odKeyAgudezavisualConNuevacorreccionId;
    }

    public void setOdKeyAgudezavisualConNuevacorreccionId(String odKeyAgudezavisualConNuevacorreccionId) {
        this.odKeyAgudezavisualConNuevacorreccionId = odKeyAgudezavisualConNuevacorreccionId;
    }

    @Basic
    @Column(name = "od_calificacion_con_nuevacorreccion_id", nullable = true)
    public String getOdCalificacionConNuevacorreccionId() {
        return odCalificacionConNuevacorreccionId;
    }

    public void setOdCalificacionConNuevacorreccionId(String odCalificacionConNuevacorreccionId) {
        this.odCalificacionConNuevacorreccionId = odCalificacionConNuevacorreccionId;
    }

    @Basic
    @Column(name = "od_sinciclopedia", nullable = true)
    public String getOdSinciclopedia() {
        return odSinciclopedia;
    }

    public void setOdSinciclopedia(String odSinciclopedia) {
        this.odSinciclopedia = odSinciclopedia;
    }

    @Basic
    @Column(name = "od_conciclopedia", nullable = true)
    public String getOdConciclopedia() {
        return odConciclopedia;
    }

    public void setOdConciclopedia(String odConciclopedia) {
        this.odConciclopedia = odConciclopedia;
    }

    @Basic
    @Column(name = "od_final", nullable = true)
    public String getOdFinal() {
        return odFinal;
    }

    public void setOdFinal(String odFinal) {
        this.odFinal = odFinal;
    }

    @Basic
    @Column(name = "od_key_agudezaadicional_id", nullable = true)
    public String getOdKeyAgudezaadicionalId() {
        return odKeyAgudezaadicionalId;
    }

    public void setOdKeyAgudezaadicionalId(String odKeyAgudezaadicionalId) {
        this.odKeyAgudezaadicionalId = odKeyAgudezaadicionalId;
    }

    @Basic
    @Column(name = "od_keratrometia", nullable = true)
    public String getOdKeratrometia() {
        return odKeratrometia;
    }

    public void setOdKeratrometia(String odKeratrometia) {
        this.odKeratrometia = odKeratrometia;
    }

    @Basic
    @Column(name = "od_lensometria", nullable = true)
    public String getOdLensometria() {
        return odLensometria;
    }

    public void setOdLensometria(String odLensometria) {
        this.odLensometria = odLensometria;
    }

    @Basic
    @Column(name = "os_key_agudezavisual_id", nullable = true)
    public String getOsKeyAgudezavisualId() {
        return osKeyAgudezavisualId;
    }

    public void setOsKeyAgudezavisualId(String osKeyAgudezavisualId) {
        this.osKeyAgudezavisualId = osKeyAgudezavisualId;
    }

    @Basic
    @Column(name = "os_calificacion_agudezavisual_id", nullable = true)
    public String getOsCalificacionAgudezavisualId() {
        return osCalificacionAgudezavisualId;
    }

    public void setOsCalificacionAgudezavisualId(String osCalificacionAgudezavisualId) {
        this.osCalificacionAgudezavisualId = osCalificacionAgudezavisualId;
    }

    @Basic
    @Column(name = "os_key_agudezavisual_con_correccion_id", nullable = true)
    public String getOsKeyAgudezavisualConCorreccionId() {
        return osKeyAgudezavisualConCorreccionId;
    }

    public void setOsKeyAgudezavisualConCorreccionId(String osKeyAgudezavisualConCorreccionId) {
        this.osKeyAgudezavisualConCorreccionId = osKeyAgudezavisualConCorreccionId;
    }

    @Basic
    @Column(name = "os_calificacion_con_correccion_id", nullable = true)
    public String getOsCalificacionConCorreccionId() {
        return osCalificacionConCorreccionId;
    }

    public void setOsCalificacionConCorreccionId(String osCalificacionConCorreccionId) {
        this.osCalificacionConCorreccionId = osCalificacionConCorreccionId;
    }

    @Basic
    @Column(name = "os_key_agudezavisual_con_nuevacorreccion_id", nullable = true)
    public String getOsKeyAgudezavisualConNuevacorreccionId() {
        return osKeyAgudezavisualConNuevacorreccionId;
    }

    public void setOsKeyAgudezavisualConNuevacorreccionId(String osKeyAgudezavisualConNuevacorreccionId) {
        this.osKeyAgudezavisualConNuevacorreccionId = osKeyAgudezavisualConNuevacorreccionId;
    }

    @Basic
    @Column(name = "os_calificacion_con_nuevacorreccion_id", nullable = true)
    public String getOsCalificacionConNuevacorreccionId() {
        return osCalificacionConNuevacorreccionId;
    }

    public void setOsCalificacionConNuevacorreccionId(String osCalificacionConNuevacorreccionId) {
        this.osCalificacionConNuevacorreccionId = osCalificacionConNuevacorreccionId;
    }

    @Basic
    @Column(name = "os_sinciclopedia", nullable = true)
    public String getOsSinciclopedia() {
        return osSinciclopedia;
    }

    public void setOsSinciclopedia(String osSinciclopedia) {
        this.osSinciclopedia = osSinciclopedia;
    }

    @Basic
    @Column(name = "os_conciclopedia", nullable = true)
    public String getOsConciclopedia() {
        return osConciclopedia;
    }

    public void setOsConciclopedia(String osConciclopedia) {
        this.osConciclopedia = osConciclopedia;
    }

    @Basic
    @Column(name = "os_final", nullable = true)
    public String getOsFinal() {
        return osFinal;
    }

    public void setOsFinal(String osFinal) {
        this.osFinal = osFinal;
    }

    @Basic
    @Column(name = "os_key_agudezaadicional_id", nullable = true)
    public String getOsKeyAgudezaadicionalId() {
        return osKeyAgudezaadicionalId;
    }

    public void setOsKeyAgudezaadicionalId(String osKeyAgudezaadicionalId) {
        this.osKeyAgudezaadicionalId = osKeyAgudezaadicionalId;
    }

    @Basic
    @Column(name = "os_keratrometia", nullable = true)
    public String getOsKeratrometia() {
        return osKeratrometia;
    }

    public void setOsKeratrometia(String osKeratrometia) {
        this.osKeratrometia = osKeratrometia;
    }

    @Basic
    @Column(name = "os_lensometria", nullable = true)
    public String getOsLensometria() {
        return osLensometria;
    }

    public void setOsLensometria(String osLensometria) {
        this.osLensometria = osLensometria;
    }

    @Basic
    @Column(name = "decerca_key_agudezavisual_id", nullable = true)
    public String getDecercaKeyAgudezavisualId() {
        return decercaKeyAgudezavisualId;
    }

    public void setDecercaKeyAgudezavisualId(String decercaKeyAgudezavisualId) {
        this.decercaKeyAgudezavisualId = decercaKeyAgudezavisualId;
    }

    @Basic
    @Column(name = "decerca_calificacion_agudezavisual_id", nullable = true)
    public String getDecercaCalificacionAgudezavisualId() {
        return decercaCalificacionAgudezavisualId;
    }

    public void setDecercaCalificacionAgudezavisualId(String decercaCalificacionAgudezavisualId) {
        this.decercaCalificacionAgudezavisualId = decercaCalificacionAgudezavisualId;
    }

    @Basic
    @Column(name = "decerca_key_agudezavisual_con_correccion_id", nullable = true)
    public String getDecercaKeyAgudezavisualConCorreccionId() {
        return decercaKeyAgudezavisualConCorreccionId;
    }

    public void setDecercaKeyAgudezavisualConCorreccionId(String decercaKeyAgudezavisualConCorreccionId) {
        this.decercaKeyAgudezavisualConCorreccionId = decercaKeyAgudezavisualConCorreccionId;
    }

    @Basic
    @Column(name = "decerca_calificacion_con_correccion_id", nullable = true)
    public String getDecercaCalificacionConCorreccionId() {
        return decercaCalificacionConCorreccionId;
    }

    public void setDecercaCalificacionConCorreccionId(String decercaCalificacionConCorreccionId) {
        this.decercaCalificacionConCorreccionId = decercaCalificacionConCorreccionId;
    }

    @Basic
    @Column(name = "decerca_key_agudezavisual_con_nuevacorreccion_id", nullable = true)
    public String getDecercaKeyAgudezavisualConNuevacorreccionId() {
        return decercaKeyAgudezavisualConNuevacorreccionId;
    }

    public void setDecercaKeyAgudezavisualConNuevacorreccionId(String decercaKeyAgudezavisualConNuevacorreccionId) {
        this.decercaKeyAgudezavisualConNuevacorreccionId = decercaKeyAgudezavisualConNuevacorreccionId;
    }

    @Basic
    @Column(name = "decerca_calificacion_con_nuevacorreccion_id", nullable = true)
    public String getDecercaCalificacionConNuevacorreccionId() {
        return decercaCalificacionConNuevacorreccionId;
    }

    public void setDecercaCalificacionConNuevacorreccionId(String decercaCalificacionConNuevacorreccionId) {
        this.decercaCalificacionConNuevacorreccionId = decercaCalificacionConNuevacorreccionId;
    }

    @Basic
    @Column(name = "decerca_sinciclopedia", nullable = true)
    public String getDecercaSinciclopedia() {
        return decercaSinciclopedia;
    }

    public void setDecercaSinciclopedia(String decercaSinciclopedia) {
        this.decercaSinciclopedia = decercaSinciclopedia;
    }

    @Basic
    @Column(name = "decerca_conciclopedia", nullable = true)
    public String getDecercaConciclopedia() {
        return decercaConciclopedia;
    }

    public void setDecercaConciclopedia(String decercaConciclopedia) {
        this.decercaConciclopedia = decercaConciclopedia;
    }

    @Basic
    @Column(name = "diagnostico1_key_diagnosticoagudeza_OD", nullable = true)
    public String getDiagnostico1KeyDiagnosticoagudezaOd() {
        return diagnostico1KeyDiagnosticoagudezaOd;
    }

    public void setDiagnostico1KeyDiagnosticoagudezaOd(String diagnostico1KeyDiagnosticoagudezaOd) {
        this.diagnostico1KeyDiagnosticoagudezaOd = diagnostico1KeyDiagnosticoagudezaOd;
    }

    @Basic
    @Column(name = "diagnostico2_key_diagnosticoagudeza_OI", nullable = true)
    public String getDiagnostico2KeyDiagnosticoagudezaOi() {
        return diagnostico2KeyDiagnosticoagudezaOi;
    }

    public void setDiagnostico2KeyDiagnosticoagudezaOi(String diagnostico2KeyDiagnosticoagudezaOi) {
        this.diagnostico2KeyDiagnosticoagudezaOi = diagnostico2KeyDiagnosticoagudezaOi;
    }

    @Basic
    @Column(name = "diagnostico3_key_diagnosticoagudeza_PRESBICIA", nullable = true)
    public String getDiagnostico3KeyDiagnosticoagudezaPresbicia() {
        return diagnostico3KeyDiagnosticoagudezaPresbicia;
    }

    public void setDiagnostico3KeyDiagnosticoagudezaPresbicia(String diagnostico3KeyDiagnosticoagudezaPresbicia) {
        this.diagnostico3KeyDiagnosticoagudezaPresbicia = diagnostico3KeyDiagnosticoagudezaPresbicia;
    }

    @Basic
    @Column(name = "observaciones_agudezavisual", nullable = true, length = 120)
    public String getObservacionesAgudezavisual() {
        return observacionesAgudezavisual;
    }

    public void setObservacionesAgudezavisual(String observacionesAgudezavisual) {
        this.observacionesAgudezavisual = observacionesAgudezavisual;
    }

    @Basic
    @Column(name = "od_motilidad1", nullable = true)
    public String getOdMotilidad1() {
        return odMotilidad1;
    }

    public void setOdMotilidad1(String odMotilidad1) {
        this.odMotilidad1 = odMotilidad1;
    }

    @Basic
    @Column(name = "od_motilidad2", nullable = true)
    public String getOdMotilidad2() {
        return odMotilidad2;
    }

    public void setOdMotilidad2(String odMotilidad2) {
        this.odMotilidad2 = odMotilidad2;
    }

    @Basic
    @Column(name = "od_motilidad3", nullable = true)
    public String getOdMotilidad3() {
        return odMotilidad3;
    }

    public void setOdMotilidad3(String odMotilidad3) {
        this.odMotilidad3 = odMotilidad3;
    }

    @Basic
    @Column(name = "od_vias_lagrimales", nullable = true)
    public String getOdViasLagrimales() {
        return odViasLagrimales;
    }

    public void setOdViasLagrimales(String odViasLagrimales) {
        this.odViasLagrimales = odViasLagrimales;
    }

    @Basic
    @Column(name = "od_parpados_pestanas", nullable = true)
    public String getOdParpadosPestanas() {
        return odParpadosPestanas;
    }

    public void setOdParpadosPestanas(String odParpadosPestanas) {
        this.odParpadosPestanas = odParpadosPestanas;
    }

    @Basic
    @Column(name = "od_conjuntiva", nullable = true)
    public String getOdConjuntiva() {
        return odConjuntiva;
    }

    public void setOdConjuntiva(String odConjuntiva) {
        this.odConjuntiva = odConjuntiva;
    }

    @Basic
    @Column(name = "od_esclera", nullable = true)
    public String getOdEsclera() {
        return odEsclera;
    }

    public void setOdEsclera(String odEsclera) {
        this.odEsclera = odEsclera;
    }

    @Basic
    @Column(name = "od_cornea", nullable = true)
    public String getOdCornea() {
        return odCornea;
    }

    public void setOdCornea(String odCornea) {
        this.odCornea = odCornea;
    }

    @Basic
    @Column(name = "od_camara_anterior", nullable = true)
    public String getOdCamaraAnterior() {
        return odCamaraAnterior;
    }

    public void setOdCamaraAnterior(String odCamaraAnterior) {
        this.odCamaraAnterior = odCamaraAnterior;
    }

    @Basic
    @Column(name = "od_iris", nullable = true)
    public String getOdIris() {
        return odIris;
    }

    public void setOdIris(String odIris) {
        this.odIris = odIris;
    }

    @Basic
    @Column(name = "od_pupila", nullable = true)
    public String getOdPupila() {
        return odPupila;
    }

    public void setOdPupila(String odPupila) {
        this.odPupila = odPupila;
    }

    @Basic
    @Column(name = "od_tio", nullable = true)
    public String getOdTio() {
        return odTio;
    }

    public void setOdTio(String odTio) {
        this.odTio = odTio;
    }

    @Basic
    @Column(name = "od_gonioscopia", nullable = true)
    public String getOdGonioscopia() {
        return odGonioscopia;
    }

    public void setOdGonioscopia(String odGonioscopia) {
        this.odGonioscopia = odGonioscopia;
    }

    @Basic
    @Column(name = "od_cristalino", nullable = true)
    public String getOdCristalino() {
        return odCristalino;
    }

    public void setOdCristalino(String odCristalino) {
        this.odCristalino = odCristalino;
    }

    @Basic
    @Column(name = "od_vitreo", nullable = true)
    public String getOdVitreo() {
        return odVitreo;
    }

    public void setOdVitreo(String odVitreo) {
        this.odVitreo = odVitreo;
    }

    @Basic
    @Column(name = "od_retina", nullable = true)
    public String getOdRetina() {
        return odRetina;
    }

    public void setOdRetina(String odRetina) {
        this.odRetina = odRetina;
    }

    @Basic
    @Column(name = "od_nervio_optico", nullable = true)
    public String getOdNervioOptico() {
        return odNervioOptico;
    }

    public void setOdNervioOptico(String odNervioOptico) {
        this.odNervioOptico = odNervioOptico;
    }

    @Basic
    @Column(name = "od_orbita", nullable = true)
    public String getOdOrbita() {
        return odOrbita;
    }

    public void setOdOrbita(String odOrbita) {
        this.odOrbita = odOrbita;
    }

    @Basic
    @Column(name = "od_observaciones", nullable = true)
    public String getOdObservaciones() {
        return odObservaciones;
    }

    public void setOdObservaciones(String odObservaciones) {
        this.odObservaciones = odObservaciones;
    }

    @Basic
    @Column(name = "os_motilidad1", nullable = true)
    public String getOsMotilidad1() {
        return osMotilidad1;
    }

    public void setOsMotilidad1(String osMotilidad1) {
        this.osMotilidad1 = osMotilidad1;
    }

    @Basic
    @Column(name = "os_motilidad2", nullable = true)
    public String getOsMotilidad2() {
        return osMotilidad2;
    }

    public void setOsMotilidad2(String osMotilidad2) {
        this.osMotilidad2 = osMotilidad2;
    }

    @Basic
    @Column(name = "os_motilidad3", nullable = true)
    public String getOsMotilidad3() {
        return osMotilidad3;
    }

    public void setOsMotilidad3(String osMotilidad3) {
        this.osMotilidad3 = osMotilidad3;
    }

    @Basic
    @Column(name = "os_vias_lagrimales", nullable = true)
    public String getOsViasLagrimales() {
        return osViasLagrimales;
    }

    public void setOsViasLagrimales(String osViasLagrimales) {
        this.osViasLagrimales = osViasLagrimales;
    }

    @Basic
    @Column(name = "os_parpados_pestanas", nullable = true)
    public String getOsParpadosPestanas() {
        return osParpadosPestanas;
    }

    public void setOsParpadosPestanas(String osParpadosPestanas) {
        this.osParpadosPestanas = osParpadosPestanas;
    }

    @Basic
    @Column(name = "os_conjuntiva", nullable = true)
    public String getOsConjuntiva() {
        return osConjuntiva;
    }

    public void setOsConjuntiva(String osConjuntiva) {
        this.osConjuntiva = osConjuntiva;
    }

    @Basic
    @Column(name = "os_esclera", nullable = true)
    public String getOsEsclera() {
        return osEsclera;
    }

    public void setOsEsclera(String osEsclera) {
        this.osEsclera = osEsclera;
    }

    @Basic
    @Column(name = "os_cornea", nullable = true)
    public String getOsCornea() {
        return osCornea;
    }

    public void setOsCornea(String osCornea) {
        this.osCornea = osCornea;
    }

    @Basic
    @Column(name = "os_camara_anterior", nullable = true)
    public String getOsCamaraAnterior() {
        return osCamaraAnterior;
    }

    public void setOsCamaraAnterior(String osCamaraAnterior) {
        this.osCamaraAnterior = osCamaraAnterior;
    }

    @Basic
    @Column(name = "os_iris", nullable = true)
    public String getOsIris() {
        return osIris;
    }

    public void setOsIris(String osIris) {
        this.osIris = osIris;
    }

    @Basic
    @Column(name = "os_pupila", nullable = true)
    public String getOsPupila() {
        return osPupila;
    }

    public void setOsPupila(String osPupila) {
        this.osPupila = osPupila;
    }

    @Basic
    @Column(name = "os_tio", nullable = true)
    public String getOsTio() {
        return osTio;
    }

    public void setOsTio(String osTio) {
        this.osTio = osTio;
    }

    @Basic
    @Column(name = "os_gonioscopia", nullable = true)
    public String getOsGonioscopia() {
        return osGonioscopia;
    }

    public void setOsGonioscopia(String osGonioscopia) {
        this.osGonioscopia = osGonioscopia;
    }

    @Basic
    @Column(name = "os_cristalino", nullable = true)
    public String getOsCristalino() {
        return osCristalino;
    }

    public void setOsCristalino(String osCristalino) {
        this.osCristalino = osCristalino;
    }

    @Basic
    @Column(name = "os_vitreo", nullable = true)
    public String getOsVitreo() {
        return osVitreo;
    }

    public void setOsVitreo(String osVitreo) {
        this.osVitreo = osVitreo;
    }

    @Basic
    @Column(name = "os_retina", nullable = true)
    public String getOsRetina() {
        return osRetina;
    }

    public void setOsRetina(String osRetina) {
        this.osRetina = osRetina;
    }

    @Basic
    @Column(name = "os_nervio_optico", nullable = true)
    public String getOsNervioOptico() {
        return osNervioOptico;
    }

    public void setOsNervioOptico(String osNervioOptico) {
        this.osNervioOptico = osNervioOptico;
    }

    @Basic
    @Column(name = "os_orbita", nullable = true)
    public String getOsOrbita() {
        return osOrbita;
    }

    public void setOsOrbita(String osOrbita) {
        this.osOrbita = osOrbita;
    }

    @Basic
    @Column(name = "os_observaciones", nullable = true)
    public String getOsObservaciones() {
        return osObservaciones;
    }

    public void setOsObservaciones(String osObservaciones) {
        this.osObservaciones = osObservaciones;
    }

    @Basic
    @Column(name = "diagnostico1_key_diagnosticoconducta_id", nullable = true, length = 160)
    public String getDiagnostico1KeyDiagnosticoconductaId() {
        return diagnostico1KeyDiagnosticoconductaId;
    }

    public void setDiagnostico1KeyDiagnosticoconductaId(String diagnostico1KeyDiagnosticoconductaId) {
        this.diagnostico1KeyDiagnosticoconductaId = diagnostico1KeyDiagnosticoconductaId;
    }

    @Basic
    @Column(name = "diagnosticoconducta_diagnostico1_key_organo_id", nullable = true)
    public String getDiagnosticoconductaDiagnostico1KeyOrganoId() {
        return diagnosticoconductaDiagnostico1KeyOrganoId;
    }

    public void setDiagnosticoconductaDiagnostico1KeyOrganoId(String diagnosticoconductaDiagnostico1KeyOrganoId) {
        this.diagnosticoconductaDiagnostico1KeyOrganoId = diagnosticoconductaDiagnostico1KeyOrganoId;
    }

    @Basic
    @Column(name = "diagnostico2_key_diagnosticoconducta_id", nullable = true, length = 160)
    public String getDiagnostico2KeyDiagnosticoconductaId() {
        return diagnostico2KeyDiagnosticoconductaId;
    }

    public void setDiagnostico2KeyDiagnosticoconductaId(String diagnostico2KeyDiagnosticoconductaId) {
        this.diagnostico2KeyDiagnosticoconductaId = diagnostico2KeyDiagnosticoconductaId;
    }

    @Basic
    @Column(name = "diagnosticoconducta_diagnostico2_key_organo_id", nullable = true)
    public String getDiagnosticoconductaDiagnostico2KeyOrganoId() {
        return diagnosticoconductaDiagnostico2KeyOrganoId;
    }

    public void setDiagnosticoconductaDiagnostico2KeyOrganoId(String diagnosticoconductaDiagnostico2KeyOrganoId) {
        this.diagnosticoconductaDiagnostico2KeyOrganoId = diagnosticoconductaDiagnostico2KeyOrganoId;
    }

    @Basic
    @Column(name = "diagnostico3_key_diagnosticoconducta_id", nullable = true, length = 160)
    public String getDiagnostico3KeyDiagnosticoconductaId() {
        return diagnostico3KeyDiagnosticoconductaId;
    }

    public void setDiagnostico3KeyDiagnosticoconductaId(String diagnostico3KeyDiagnosticoconductaId) {
        this.diagnostico3KeyDiagnosticoconductaId = diagnostico3KeyDiagnosticoconductaId;
    }

    @Basic
    @Column(name = "diagnosticoconducta_diagnostico3_key_organo_id", nullable = true)
    public String getDiagnosticoconductaDiagnostico3KeyOrganoId() {
        return diagnosticoconductaDiagnostico3KeyOrganoId;
    }

    public void setDiagnosticoconductaDiagnostico3KeyOrganoId(String diagnosticoconductaDiagnostico3KeyOrganoId) {
        this.diagnosticoconductaDiagnostico3KeyOrganoId = diagnosticoconductaDiagnostico3KeyOrganoId;
    }

    @Basic
    @Column(name = "diagnostico4_key_diagnosticoconducta_id", nullable = true, length = 160)
    public String getDiagnostico4KeyDiagnosticoconductaId() {
        return diagnostico4KeyDiagnosticoconductaId;
    }

    public void setDiagnostico4KeyDiagnosticoconductaId(String diagnostico4KeyDiagnosticoconductaId) {
        this.diagnostico4KeyDiagnosticoconductaId = diagnostico4KeyDiagnosticoconductaId;
    }

    @Basic
    @Column(name = "diagnosticoconducta_diagnostico4_key_organo_id", nullable = true)
    public String getDiagnosticoconductaDiagnostico4KeyOrganoId() {
        return diagnosticoconductaDiagnostico4KeyOrganoId;
    }

    public void setDiagnosticoconductaDiagnostico4KeyOrganoId(String diagnosticoconductaDiagnostico4KeyOrganoId) {
        this.diagnosticoconductaDiagnostico4KeyOrganoId = diagnosticoconductaDiagnostico4KeyOrganoId;
    }

    @Basic
    @Column(name = "diagnostico5_key_diagnosticoconducta_id", nullable = true, length = 160)
    public String getDiagnostico5KeyDiagnosticoconductaId() {
        return diagnostico5KeyDiagnosticoconductaId;
    }

    public void setDiagnostico5KeyDiagnosticoconductaId(String diagnostico5KeyDiagnosticoconductaId) {
        this.diagnostico5KeyDiagnosticoconductaId = diagnostico5KeyDiagnosticoconductaId;
    }

    @Basic
    @Column(name = "diagnosticoconducta_diagnostico5_key_organo_id", nullable = true)
    public String getDiagnosticoconductaDiagnostico5KeyOrganoId() {
        return diagnosticoconductaDiagnostico5KeyOrganoId;
    }

    public void setDiagnosticoconductaDiagnostico5KeyOrganoId(String diagnosticoconductaDiagnostico5KeyOrganoId) {
        this.diagnosticoconductaDiagnostico5KeyOrganoId = diagnosticoconductaDiagnostico5KeyOrganoId;
    }

    @Basic
    @Column(name = "diagnostico_observaciones_conducta", nullable = true)
    public String getDiagnosticoObservacionesConducta() {
        return diagnosticoObservacionesConducta;
    }

    public void setDiagnosticoObservacionesConducta(String diagnosticoObservacionesConducta) {
        this.diagnosticoObservacionesConducta = diagnosticoObservacionesConducta;
    }

    @Basic
    @Column(name = "diagnostico_conducta_texto", nullable = true)
    public String getDiagnosticoConductaTexto() {
        return diagnosticoConductaTexto;
    }

    public void setDiagnosticoConductaTexto(String diagnosticoConductaTexto) {
        this.diagnosticoConductaTexto = diagnosticoConductaTexto;
    }

    @Basic
    @Column(name = "md_estado_operaciones", nullable = true, length = 1)
    public String getMdEstadoOperaciones() {
        return mdEstadoOperaciones;
    }

    public void setMdEstadoOperaciones(String mdEstadoOperaciones) {
        this.mdEstadoOperaciones = mdEstadoOperaciones;
    }

    @Basic
    @Column(name = "md_quirofano", nullable = true)
    public String getMdQuirofano() {
        return mdQuirofano;
    }

    public void setMdQuirofano(String mdQuirofano) {
        this.mdQuirofano = mdQuirofano;
    }

    @Basic
    @Column(name = "md_cirujano", nullable = true, length = 60)
    public String getMdCirujano() {
        return mdCirujano;
    }

    public void setMdCirujano(String mdCirujano) {
        this.mdCirujano = mdCirujano;
    }

    @Basic
    @Column(name = "key_tipoanestesia_id", nullable = true)
    public String getKeyTipoanestesiaId() {
        return keyTipoanestesiaId;
    }

    public void setKeyTipoanestesiaId(String keyTipoanestesiaId) {
        this.keyTipoanestesiaId = keyTipoanestesiaId;
    }

    @Basic
    @Column(name = "md_anesteciologo", nullable = true)
    public String getMdAnesteciologo() {
        return mdAnesteciologo;
    }

    public void setMdAnesteciologo(String mdAnesteciologo) {
        this.mdAnesteciologo = mdAnesteciologo;
    }

    @Basic
    @Column(name = "key_cirugia_id", nullable = true)
    public String getKeyCirugiaId() {
        return keyCirugiaId;
    }

    public void setKeyCirugiaId(String keyCirugiaId) {
        this.keyCirugiaId = keyCirugiaId;
    }

    @Basic
    @Column(name = "md_ayudante", nullable = true)
    public String getMdAyudante() {
        return mdAyudante;
    }

    public void setMdAyudante(String mdAyudante) {
        this.mdAyudante = mdAyudante;
    }

    @Basic
    @Column(name = "md_horainicio", nullable = true)
    public String getMdHorainicio() {
        return mdHorainicio;
    }

    public void setMdHorainicio(String mdHorainicio) {
        this.mdHorainicio = mdHorainicio;
    }

    @Basic
    @Column(name = "md_horafinalizacion", nullable = true)
    public String getMdHorafinalizacion() {
        return mdHorafinalizacion;
    }

    public void setMdHorafinalizacion(String mdHorafinalizacion) {
        this.mdHorafinalizacion = mdHorafinalizacion;
    }

    @Basic
    @Column(name = "key_lio_id", nullable = true)
    public String getKeyLioId() {
        return keyLioId;
    }

    public void setKeyLioId(String keyLioId) {
        this.keyLioId = keyLioId;
    }

    @Basic
    @Column(name = "sala_operaciones_observaciones", nullable = true)
    public String getSalaOperacionesObservaciones() {
        return salaOperacionesObservaciones;
    }

    public void setSalaOperacionesObservaciones(String salaOperacionesObservaciones) {
        this.salaOperacionesObservaciones = salaOperacionesObservaciones;
    }

    @Basic
    @Column(name = "sala_operaciones_motivo", nullable = true)
    public String getSalaOperacionesMotivo() {
        return salaOperacionesMotivo;
    }

    public void setSalaOperacionesMotivo(String salaOperacionesMotivo) {
        this.salaOperacionesMotivo = salaOperacionesMotivo;
    }

    @Basic
    @Column(name = "sala_operaciones_diagnostico", nullable = true)
    public String getSalaOperacionesDiagnostico() {
        return salaOperacionesDiagnostico;
    }

    public void setSalaOperacionesDiagnostico(String salaOperacionesDiagnostico) {
        this.salaOperacionesDiagnostico = salaOperacionesDiagnostico;
    }

    @Basic
    @Column(name = "Esfera_OD", nullable = true)
    public String getEsferaOd() {
        return esferaOd;
    }

    public void setEsferaOd(String esferaOd) {
        this.esferaOd = esferaOd;
    }

    @Basic
    @Column(name = "Cilindro_OD", nullable = true)
    public String getCilindroOd() {
        return cilindroOd;
    }

    public void setCilindroOd(String cilindroOd) {
        this.cilindroOd = cilindroOd;
    }

    @Basic
    @Column(name = "Eje_OD", nullable = true)
    public String getEjeOd() {
        return ejeOd;
    }

    public void setEjeOd(String ejeOd) {
        this.ejeOd = ejeOd;
    }

    @Basic
    @Column(name = "Prisma_OD", nullable = true)
    public String getPrismaOd() {
        return prismaOd;
    }

    public void setPrismaOd(String prismaOd) {
        this.prismaOd = prismaOd;
    }

    @Basic
    @Column(name = "adicion_OD", nullable = true)
    public String getAdicionOd() {
        return adicionOd;
    }

    public void setAdicionOd(String adicionOd) {
        this.adicionOd = adicionOd;
    }

    @Basic
    @Column(name = "dip_OD", nullable = true)
    public String getDipOd() {
        return dipOd;
    }

    public void setDipOd(String dipOd) {
        this.dipOd = dipOd;
    }

    @Basic
    @Column(name = "Esfera_OI", nullable = true)
    public String getEsferaOi() {
        return esferaOi;
    }

    public void setEsferaOi(String esferaOi) {
        this.esferaOi = esferaOi;
    }

    @Basic
    @Column(name = "Cilindro_OI", nullable = true)
    public String getCilindroOi() {
        return cilindroOi;
    }

    public void setCilindroOi(String cilindroOi) {
        this.cilindroOi = cilindroOi;
    }

    @Basic
    @Column(name = "Eje_OI", nullable = true)
    public String getEjeOi() {
        return ejeOi;
    }

    public void setEjeOi(String ejeOi) {
        this.ejeOi = ejeOi;
    }

    @Basic
    @Column(name = "Prisma_OI", nullable = true)
    public String getPrismaOi() {
        return prismaOi;
    }

    public void setPrismaOi(String prismaOi) {
        this.prismaOi = prismaOi;
    }

    @Basic
    @Column(name = "adicion_OI", nullable = true)
    public String getAdicionOi() {
        return adicionOi;
    }

    public void setAdicionOi(String adicionOi) {
        this.adicionOi = adicionOi;
    }

    @Basic
    @Column(name = "dip_OI", nullable = true)
    public String getDipOi() {
        return dipOi;
    }

    public void setDipOi(String dipOi) {
        this.dipOi = dipOi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DiagnosticoMedico that = (DiagnosticoMedico) o;

        if (mdKeyPacienteId != null ? !mdKeyPacienteId.equals(that.mdKeyPacienteId) : that.mdKeyPacienteId != null)
            return false;
        if (mdFechaProceso != null ? !mdFechaProceso.equals(that.mdFechaProceso) : that.mdFechaProceso != null)
            return false;
        if (mdHoraProceso != null ? !mdHoraProceso.equals(that.mdHoraProceso) : that.mdHoraProceso != null)
            return false;
        if (mdEstadoDiagnostico != null ? !mdEstadoDiagnostico.equals(that.mdEstadoDiagnostico) : that.mdEstadoDiagnostico != null)
            return false;
        if (mdMotivosDeConsulta != null ? !mdMotivosDeConsulta.equals(that.mdMotivosDeConsulta) : that.mdMotivosDeConsulta != null)
            return false;
        if (mdAntecedentesPersonales != null ? !mdAntecedentesPersonales.equals(that.mdAntecedentesPersonales) : that.mdAntecedentesPersonales != null)
            return false;
        if (mdAntecedentesFamiliares != null ? !mdAntecedentesFamiliares.equals(that.mdAntecedentesFamiliares) : that.mdAntecedentesFamiliares != null)
            return false;
        if (mdRevisionConsulta != null ? !mdRevisionConsulta.equals(that.mdRevisionConsulta) : that.mdRevisionConsulta != null)
            return false;
        if (mdCirugiasPrevias != null ? !mdCirugiasPrevias.equals(that.mdCirugiasPrevias) : that.mdCirugiasPrevias != null)
            return false;
        if (odKeyAgudezavisualId != null ? !odKeyAgudezavisualId.equals(that.odKeyAgudezavisualId) : that.odKeyAgudezavisualId != null)
            return false;
        if (odCalificacionAgudezavisualId != null ? !odCalificacionAgudezavisualId.equals(that.odCalificacionAgudezavisualId) : that.odCalificacionAgudezavisualId != null)
            return false;
        if (odKeyAgudezavisualConCorreccionId != null ? !odKeyAgudezavisualConCorreccionId.equals(that.odKeyAgudezavisualConCorreccionId) : that.odKeyAgudezavisualConCorreccionId != null)
            return false;
        if (odCalificacionConCorreccionId != null ? !odCalificacionConCorreccionId.equals(that.odCalificacionConCorreccionId) : that.odCalificacionConCorreccionId != null)
            return false;
        if (odKeyAgudezavisualConNuevacorreccionId != null ? !odKeyAgudezavisualConNuevacorreccionId.equals(that.odKeyAgudezavisualConNuevacorreccionId) : that.odKeyAgudezavisualConNuevacorreccionId != null)
            return false;
        if (odCalificacionConNuevacorreccionId != null ? !odCalificacionConNuevacorreccionId.equals(that.odCalificacionConNuevacorreccionId) : that.odCalificacionConNuevacorreccionId != null)
            return false;
        if (odSinciclopedia != null ? !odSinciclopedia.equals(that.odSinciclopedia) : that.odSinciclopedia != null)
            return false;
        if (odConciclopedia != null ? !odConciclopedia.equals(that.odConciclopedia) : that.odConciclopedia != null)
            return false;
        if (odFinal != null ? !odFinal.equals(that.odFinal) : that.odFinal != null) return false;
        if (odKeyAgudezaadicionalId != null ? !odKeyAgudezaadicionalId.equals(that.odKeyAgudezaadicionalId) : that.odKeyAgudezaadicionalId != null)
            return false;
        if (odKeratrometia != null ? !odKeratrometia.equals(that.odKeratrometia) : that.odKeratrometia != null)
            return false;
        if (odLensometria != null ? !odLensometria.equals(that.odLensometria) : that.odLensometria != null)
            return false;
        if (osKeyAgudezavisualId != null ? !osKeyAgudezavisualId.equals(that.osKeyAgudezavisualId) : that.osKeyAgudezavisualId != null)
            return false;
        if (osCalificacionAgudezavisualId != null ? !osCalificacionAgudezavisualId.equals(that.osCalificacionAgudezavisualId) : that.osCalificacionAgudezavisualId != null)
            return false;
        if (osKeyAgudezavisualConCorreccionId != null ? !osKeyAgudezavisualConCorreccionId.equals(that.osKeyAgudezavisualConCorreccionId) : that.osKeyAgudezavisualConCorreccionId != null)
            return false;
        if (osCalificacionConCorreccionId != null ? !osCalificacionConCorreccionId.equals(that.osCalificacionConCorreccionId) : that.osCalificacionConCorreccionId != null)
            return false;
        if (osKeyAgudezavisualConNuevacorreccionId != null ? !osKeyAgudezavisualConNuevacorreccionId.equals(that.osKeyAgudezavisualConNuevacorreccionId) : that.osKeyAgudezavisualConNuevacorreccionId != null)
            return false;
        if (osCalificacionConNuevacorreccionId != null ? !osCalificacionConNuevacorreccionId.equals(that.osCalificacionConNuevacorreccionId) : that.osCalificacionConNuevacorreccionId != null)
            return false;
        if (osSinciclopedia != null ? !osSinciclopedia.equals(that.osSinciclopedia) : that.osSinciclopedia != null)
            return false;
        if (osConciclopedia != null ? !osConciclopedia.equals(that.osConciclopedia) : that.osConciclopedia != null)
            return false;
        if (osFinal != null ? !osFinal.equals(that.osFinal) : that.osFinal != null) return false;
        if (osKeyAgudezaadicionalId != null ? !osKeyAgudezaadicionalId.equals(that.osKeyAgudezaadicionalId) : that.osKeyAgudezaadicionalId != null)
            return false;
        if (osKeratrometia != null ? !osKeratrometia.equals(that.osKeratrometia) : that.osKeratrometia != null)
            return false;
        if (osLensometria != null ? !osLensometria.equals(that.osLensometria) : that.osLensometria != null)
            return false;
        if (decercaKeyAgudezavisualId != null ? !decercaKeyAgudezavisualId.equals(that.decercaKeyAgudezavisualId) : that.decercaKeyAgudezavisualId != null)
            return false;
        if (decercaCalificacionAgudezavisualId != null ? !decercaCalificacionAgudezavisualId.equals(that.decercaCalificacionAgudezavisualId) : that.decercaCalificacionAgudezavisualId != null)
            return false;
        if (decercaKeyAgudezavisualConCorreccionId != null ? !decercaKeyAgudezavisualConCorreccionId.equals(that.decercaKeyAgudezavisualConCorreccionId) : that.decercaKeyAgudezavisualConCorreccionId != null)
            return false;
        if (decercaCalificacionConCorreccionId != null ? !decercaCalificacionConCorreccionId.equals(that.decercaCalificacionConCorreccionId) : that.decercaCalificacionConCorreccionId != null)
            return false;
        if (decercaKeyAgudezavisualConNuevacorreccionId != null ? !decercaKeyAgudezavisualConNuevacorreccionId.equals(that.decercaKeyAgudezavisualConNuevacorreccionId) : that.decercaKeyAgudezavisualConNuevacorreccionId != null)
            return false;
        if (decercaCalificacionConNuevacorreccionId != null ? !decercaCalificacionConNuevacorreccionId.equals(that.decercaCalificacionConNuevacorreccionId) : that.decercaCalificacionConNuevacorreccionId != null)
            return false;
        if (decercaSinciclopedia != null ? !decercaSinciclopedia.equals(that.decercaSinciclopedia) : that.decercaSinciclopedia != null)
            return false;
        if (decercaConciclopedia != null ? !decercaConciclopedia.equals(that.decercaConciclopedia) : that.decercaConciclopedia != null)
            return false;
        if (diagnostico1KeyDiagnosticoagudezaOd != null ? !diagnostico1KeyDiagnosticoagudezaOd.equals(that.diagnostico1KeyDiagnosticoagudezaOd) : that.diagnostico1KeyDiagnosticoagudezaOd != null)
            return false;
        if (diagnostico2KeyDiagnosticoagudezaOi != null ? !diagnostico2KeyDiagnosticoagudezaOi.equals(that.diagnostico2KeyDiagnosticoagudezaOi) : that.diagnostico2KeyDiagnosticoagudezaOi != null)
            return false;
        if (diagnostico3KeyDiagnosticoagudezaPresbicia != null ? !diagnostico3KeyDiagnosticoagudezaPresbicia.equals(that.diagnostico3KeyDiagnosticoagudezaPresbicia) : that.diagnostico3KeyDiagnosticoagudezaPresbicia != null)
            return false;
        if (observacionesAgudezavisual != null ? !observacionesAgudezavisual.equals(that.observacionesAgudezavisual) : that.observacionesAgudezavisual != null)
            return false;
        if (odMotilidad1 != null ? !odMotilidad1.equals(that.odMotilidad1) : that.odMotilidad1 != null) return false;
        if (odMotilidad2 != null ? !odMotilidad2.equals(that.odMotilidad2) : that.odMotilidad2 != null) return false;
        if (odMotilidad3 != null ? !odMotilidad3.equals(that.odMotilidad3) : that.odMotilidad3 != null) return false;
        if (odViasLagrimales != null ? !odViasLagrimales.equals(that.odViasLagrimales) : that.odViasLagrimales != null)
            return false;
        if (odParpadosPestanas != null ? !odParpadosPestanas.equals(that.odParpadosPestanas) : that.odParpadosPestanas != null)
            return false;
        if (odConjuntiva != null ? !odConjuntiva.equals(that.odConjuntiva) : that.odConjuntiva != null) return false;
        if (odEsclera != null ? !odEsclera.equals(that.odEsclera) : that.odEsclera != null) return false;
        if (odCornea != null ? !odCornea.equals(that.odCornea) : that.odCornea != null) return false;
        if (odCamaraAnterior != null ? !odCamaraAnterior.equals(that.odCamaraAnterior) : that.odCamaraAnterior != null)
            return false;
        if (odIris != null ? !odIris.equals(that.odIris) : that.odIris != null) return false;
        if (odPupila != null ? !odPupila.equals(that.odPupila) : that.odPupila != null) return false;
        if (odTio != null ? !odTio.equals(that.odTio) : that.odTio != null) return false;
        if (odGonioscopia != null ? !odGonioscopia.equals(that.odGonioscopia) : that.odGonioscopia != null)
            return false;
        if (odCristalino != null ? !odCristalino.equals(that.odCristalino) : that.odCristalino != null) return false;
        if (odVitreo != null ? !odVitreo.equals(that.odVitreo) : that.odVitreo != null) return false;
        if (odRetina != null ? !odRetina.equals(that.odRetina) : that.odRetina != null) return false;
        if (odNervioOptico != null ? !odNervioOptico.equals(that.odNervioOptico) : that.odNervioOptico != null)
            return false;
        if (odOrbita != null ? !odOrbita.equals(that.odOrbita) : that.odOrbita != null) return false;
        if (odObservaciones != null ? !odObservaciones.equals(that.odObservaciones) : that.odObservaciones != null)
            return false;
        if (osMotilidad1 != null ? !osMotilidad1.equals(that.osMotilidad1) : that.osMotilidad1 != null) return false;
        if (osMotilidad2 != null ? !osMotilidad2.equals(that.osMotilidad2) : that.osMotilidad2 != null) return false;
        if (osMotilidad3 != null ? !osMotilidad3.equals(that.osMotilidad3) : that.osMotilidad3 != null) return false;
        if (osViasLagrimales != null ? !osViasLagrimales.equals(that.osViasLagrimales) : that.osViasLagrimales != null)
            return false;
        if (osParpadosPestanas != null ? !osParpadosPestanas.equals(that.osParpadosPestanas) : that.osParpadosPestanas != null)
            return false;
        if (osConjuntiva != null ? !osConjuntiva.equals(that.osConjuntiva) : that.osConjuntiva != null) return false;
        if (osEsclera != null ? !osEsclera.equals(that.osEsclera) : that.osEsclera != null) return false;
        if (osCornea != null ? !osCornea.equals(that.osCornea) : that.osCornea != null) return false;
        if (osCamaraAnterior != null ? !osCamaraAnterior.equals(that.osCamaraAnterior) : that.osCamaraAnterior != null)
            return false;
        if (osIris != null ? !osIris.equals(that.osIris) : that.osIris != null) return false;
        if (osPupila != null ? !osPupila.equals(that.osPupila) : that.osPupila != null) return false;
        if (osTio != null ? !osTio.equals(that.osTio) : that.osTio != null) return false;
        if (osGonioscopia != null ? !osGonioscopia.equals(that.osGonioscopia) : that.osGonioscopia != null)
            return false;
        if (osCristalino != null ? !osCristalino.equals(that.osCristalino) : that.osCristalino != null) return false;
        if (osVitreo != null ? !osVitreo.equals(that.osVitreo) : that.osVitreo != null) return false;
        if (osRetina != null ? !osRetina.equals(that.osRetina) : that.osRetina != null) return false;
        if (osNervioOptico != null ? !osNervioOptico.equals(that.osNervioOptico) : that.osNervioOptico != null)
            return false;
        if (osOrbita != null ? !osOrbita.equals(that.osOrbita) : that.osOrbita != null) return false;
        if (osObservaciones != null ? !osObservaciones.equals(that.osObservaciones) : that.osObservaciones != null)
            return false;
        if (diagnostico1KeyDiagnosticoconductaId != null ? !diagnostico1KeyDiagnosticoconductaId.equals(that.diagnostico1KeyDiagnosticoconductaId) : that.diagnostico1KeyDiagnosticoconductaId != null)
            return false;
        if (diagnosticoconductaDiagnostico1KeyOrganoId != null ? !diagnosticoconductaDiagnostico1KeyOrganoId.equals(that.diagnosticoconductaDiagnostico1KeyOrganoId) : that.diagnosticoconductaDiagnostico1KeyOrganoId != null)
            return false;
        if (diagnostico2KeyDiagnosticoconductaId != null ? !diagnostico2KeyDiagnosticoconductaId.equals(that.diagnostico2KeyDiagnosticoconductaId) : that.diagnostico2KeyDiagnosticoconductaId != null)
            return false;
        if (diagnosticoconductaDiagnostico2KeyOrganoId != null ? !diagnosticoconductaDiagnostico2KeyOrganoId.equals(that.diagnosticoconductaDiagnostico2KeyOrganoId) : that.diagnosticoconductaDiagnostico2KeyOrganoId != null)
            return false;
        if (diagnostico3KeyDiagnosticoconductaId != null ? !diagnostico3KeyDiagnosticoconductaId.equals(that.diagnostico3KeyDiagnosticoconductaId) : that.diagnostico3KeyDiagnosticoconductaId != null)
            return false;
        if (diagnosticoconductaDiagnostico3KeyOrganoId != null ? !diagnosticoconductaDiagnostico3KeyOrganoId.equals(that.diagnosticoconductaDiagnostico3KeyOrganoId) : that.diagnosticoconductaDiagnostico3KeyOrganoId != null)
            return false;
        if (diagnostico4KeyDiagnosticoconductaId != null ? !diagnostico4KeyDiagnosticoconductaId.equals(that.diagnostico4KeyDiagnosticoconductaId) : that.diagnostico4KeyDiagnosticoconductaId != null)
            return false;
        if (diagnosticoconductaDiagnostico4KeyOrganoId != null ? !diagnosticoconductaDiagnostico4KeyOrganoId.equals(that.diagnosticoconductaDiagnostico4KeyOrganoId) : that.diagnosticoconductaDiagnostico4KeyOrganoId != null)
            return false;
        if (diagnostico5KeyDiagnosticoconductaId != null ? !diagnostico5KeyDiagnosticoconductaId.equals(that.diagnostico5KeyDiagnosticoconductaId) : that.diagnostico5KeyDiagnosticoconductaId != null)
            return false;
        if (diagnosticoconductaDiagnostico5KeyOrganoId != null ? !diagnosticoconductaDiagnostico5KeyOrganoId.equals(that.diagnosticoconductaDiagnostico5KeyOrganoId) : that.diagnosticoconductaDiagnostico5KeyOrganoId != null)
            return false;
        if (diagnosticoObservacionesConducta != null ? !diagnosticoObservacionesConducta.equals(that.diagnosticoObservacionesConducta) : that.diagnosticoObservacionesConducta != null)
            return false;
        if (diagnosticoConductaTexto != null ? !diagnosticoConductaTexto.equals(that.diagnosticoConductaTexto) : that.diagnosticoConductaTexto != null)
            return false;
        if (mdEstadoOperaciones != null ? !mdEstadoOperaciones.equals(that.mdEstadoOperaciones) : that.mdEstadoOperaciones != null)
            return false;
        if (mdQuirofano != null ? !mdQuirofano.equals(that.mdQuirofano) : that.mdQuirofano != null) return false;
        if (mdCirujano != null ? !mdCirujano.equals(that.mdCirujano) : that.mdCirujano != null) return false;
        if (keyTipoanestesiaId != null ? !keyTipoanestesiaId.equals(that.keyTipoanestesiaId) : that.keyTipoanestesiaId != null)
            return false;
        if (mdAnesteciologo != null ? !mdAnesteciologo.equals(that.mdAnesteciologo) : that.mdAnesteciologo != null)
            return false;
        if (keyCirugiaId != null ? !keyCirugiaId.equals(that.keyCirugiaId) : that.keyCirugiaId != null) return false;
        if (mdAyudante != null ? !mdAyudante.equals(that.mdAyudante) : that.mdAyudante != null) return false;
        if (mdHorainicio != null ? !mdHorainicio.equals(that.mdHorainicio) : that.mdHorainicio != null) return false;
        if (mdHorafinalizacion != null ? !mdHorafinalizacion.equals(that.mdHorafinalizacion) : that.mdHorafinalizacion != null)
            return false;
        if (keyLioId != null ? !keyLioId.equals(that.keyLioId) : that.keyLioId != null) return false;
        if (salaOperacionesObservaciones != null ? !salaOperacionesObservaciones.equals(that.salaOperacionesObservaciones) : that.salaOperacionesObservaciones != null)
            return false;
        if (salaOperacionesMotivo != null ? !salaOperacionesMotivo.equals(that.salaOperacionesMotivo) : that.salaOperacionesMotivo != null)
            return false;
        if (salaOperacionesDiagnostico != null ? !salaOperacionesDiagnostico.equals(that.salaOperacionesDiagnostico) : that.salaOperacionesDiagnostico != null)
            return false;
        if (esferaOd != null ? !esferaOd.equals(that.esferaOd) : that.esferaOd != null) return false;
        if (cilindroOd != null ? !cilindroOd.equals(that.cilindroOd) : that.cilindroOd != null) return false;
        if (ejeOd != null ? !ejeOd.equals(that.ejeOd) : that.ejeOd != null) return false;
        if (prismaOd != null ? !prismaOd.equals(that.prismaOd) : that.prismaOd != null) return false;
        if (adicionOd != null ? !adicionOd.equals(that.adicionOd) : that.adicionOd != null) return false;
        if (dipOd != null ? !dipOd.equals(that.dipOd) : that.dipOd != null) return false;
        if (esferaOi != null ? !esferaOi.equals(that.esferaOi) : that.esferaOi != null) return false;
        if (cilindroOi != null ? !cilindroOi.equals(that.cilindroOi) : that.cilindroOi != null) return false;
        if (ejeOi != null ? !ejeOi.equals(that.ejeOi) : that.ejeOi != null) return false;
        if (prismaOi != null ? !prismaOi.equals(that.prismaOi) : that.prismaOi != null) return false;
        if (adicionOi != null ? !adicionOi.equals(that.adicionOi) : that.adicionOi != null) return false;
        if (dipOi != null ? !dipOi.equals(that.dipOi) : that.dipOi != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = mdKeyPacienteId != null ? mdKeyPacienteId.hashCode() : 0;
        result = 31 * result + (mdFechaProceso != null ? mdFechaProceso.hashCode() : 0);
        result = 31 * result + (mdHoraProceso != null ? mdHoraProceso.hashCode() : 0);
        result = 31 * result + (mdEstadoDiagnostico != null ? mdEstadoDiagnostico.hashCode() : 0);
        result = 31 * result + (mdMotivosDeConsulta != null ? mdMotivosDeConsulta.hashCode() : 0);
        result = 31 * result + (mdAntecedentesPersonales != null ? mdAntecedentesPersonales.hashCode() : 0);
        result = 31 * result + (mdAntecedentesFamiliares != null ? mdAntecedentesFamiliares.hashCode() : 0);
        result = 31 * result + (mdRevisionConsulta != null ? mdRevisionConsulta.hashCode() : 0);
        result = 31 * result + (mdCirugiasPrevias != null ? mdCirugiasPrevias.hashCode() : 0);
        result = 31 * result + (odKeyAgudezavisualId != null ? odKeyAgudezavisualId.hashCode() : 0);
        result = 31 * result + (odCalificacionAgudezavisualId != null ? odCalificacionAgudezavisualId.hashCode() : 0);
        result = 31 * result + (odKeyAgudezavisualConCorreccionId != null ? odKeyAgudezavisualConCorreccionId.hashCode() : 0);
        result = 31 * result + (odCalificacionConCorreccionId != null ? odCalificacionConCorreccionId.hashCode() : 0);
        result = 31 * result + (odKeyAgudezavisualConNuevacorreccionId != null ? odKeyAgudezavisualConNuevacorreccionId.hashCode() : 0);
        result = 31 * result + (odCalificacionConNuevacorreccionId != null ? odCalificacionConNuevacorreccionId.hashCode() : 0);
        result = 31 * result + (odSinciclopedia != null ? odSinciclopedia.hashCode() : 0);
        result = 31 * result + (odConciclopedia != null ? odConciclopedia.hashCode() : 0);
        result = 31 * result + (odFinal != null ? odFinal.hashCode() : 0);
        result = 31 * result + (odKeyAgudezaadicionalId != null ? odKeyAgudezaadicionalId.hashCode() : 0);
        result = 31 * result + (odKeratrometia != null ? odKeratrometia.hashCode() : 0);
        result = 31 * result + (odLensometria != null ? odLensometria.hashCode() : 0);
        result = 31 * result + (osKeyAgudezavisualId != null ? osKeyAgudezavisualId.hashCode() : 0);
        result = 31 * result + (osCalificacionAgudezavisualId != null ? osCalificacionAgudezavisualId.hashCode() : 0);
        result = 31 * result + (osKeyAgudezavisualConCorreccionId != null ? osKeyAgudezavisualConCorreccionId.hashCode() : 0);
        result = 31 * result + (osCalificacionConCorreccionId != null ? osCalificacionConCorreccionId.hashCode() : 0);
        result = 31 * result + (osKeyAgudezavisualConNuevacorreccionId != null ? osKeyAgudezavisualConNuevacorreccionId.hashCode() : 0);
        result = 31 * result + (osCalificacionConNuevacorreccionId != null ? osCalificacionConNuevacorreccionId.hashCode() : 0);
        result = 31 * result + (osSinciclopedia != null ? osSinciclopedia.hashCode() : 0);
        result = 31 * result + (osConciclopedia != null ? osConciclopedia.hashCode() : 0);
        result = 31 * result + (osFinal != null ? osFinal.hashCode() : 0);
        result = 31 * result + (osKeyAgudezaadicionalId != null ? osKeyAgudezaadicionalId.hashCode() : 0);
        result = 31 * result + (osKeratrometia != null ? osKeratrometia.hashCode() : 0);
        result = 31 * result + (osLensometria != null ? osLensometria.hashCode() : 0);
        result = 31 * result + (decercaKeyAgudezavisualId != null ? decercaKeyAgudezavisualId.hashCode() : 0);
        result = 31 * result + (decercaCalificacionAgudezavisualId != null ? decercaCalificacionAgudezavisualId.hashCode() : 0);
        result = 31 * result + (decercaKeyAgudezavisualConCorreccionId != null ? decercaKeyAgudezavisualConCorreccionId.hashCode() : 0);
        result = 31 * result + (decercaCalificacionConCorreccionId != null ? decercaCalificacionConCorreccionId.hashCode() : 0);
        result = 31 * result + (decercaKeyAgudezavisualConNuevacorreccionId != null ? decercaKeyAgudezavisualConNuevacorreccionId.hashCode() : 0);
        result = 31 * result + (decercaCalificacionConNuevacorreccionId != null ? decercaCalificacionConNuevacorreccionId.hashCode() : 0);
        result = 31 * result + (decercaSinciclopedia != null ? decercaSinciclopedia.hashCode() : 0);
        result = 31 * result + (decercaConciclopedia != null ? decercaConciclopedia.hashCode() : 0);
        result = 31 * result + (diagnostico1KeyDiagnosticoagudezaOd != null ? diagnostico1KeyDiagnosticoagudezaOd.hashCode() : 0);
        result = 31 * result + (diagnostico2KeyDiagnosticoagudezaOi != null ? diagnostico2KeyDiagnosticoagudezaOi.hashCode() : 0);
        result = 31 * result + (diagnostico3KeyDiagnosticoagudezaPresbicia != null ? diagnostico3KeyDiagnosticoagudezaPresbicia.hashCode() : 0);
        result = 31 * result + (observacionesAgudezavisual != null ? observacionesAgudezavisual.hashCode() : 0);
        result = 31 * result + (odMotilidad1 != null ? odMotilidad1.hashCode() : 0);
        result = 31 * result + (odMotilidad2 != null ? odMotilidad2.hashCode() : 0);
        result = 31 * result + (odMotilidad3 != null ? odMotilidad3.hashCode() : 0);
        result = 31 * result + (odViasLagrimales != null ? odViasLagrimales.hashCode() : 0);
        result = 31 * result + (odParpadosPestanas != null ? odParpadosPestanas.hashCode() : 0);
        result = 31 * result + (odConjuntiva != null ? odConjuntiva.hashCode() : 0);
        result = 31 * result + (odEsclera != null ? odEsclera.hashCode() : 0);
        result = 31 * result + (odCornea != null ? odCornea.hashCode() : 0);
        result = 31 * result + (odCamaraAnterior != null ? odCamaraAnterior.hashCode() : 0);
        result = 31 * result + (odIris != null ? odIris.hashCode() : 0);
        result = 31 * result + (odPupila != null ? odPupila.hashCode() : 0);
        result = 31 * result + (odTio != null ? odTio.hashCode() : 0);
        result = 31 * result + (odGonioscopia != null ? odGonioscopia.hashCode() : 0);
        result = 31 * result + (odCristalino != null ? odCristalino.hashCode() : 0);
        result = 31 * result + (odVitreo != null ? odVitreo.hashCode() : 0);
        result = 31 * result + (odRetina != null ? odRetina.hashCode() : 0);
        result = 31 * result + (odNervioOptico != null ? odNervioOptico.hashCode() : 0);
        result = 31 * result + (odOrbita != null ? odOrbita.hashCode() : 0);
        result = 31 * result + (odObservaciones != null ? odObservaciones.hashCode() : 0);
        result = 31 * result + (osMotilidad1 != null ? osMotilidad1.hashCode() : 0);
        result = 31 * result + (osMotilidad2 != null ? osMotilidad2.hashCode() : 0);
        result = 31 * result + (osMotilidad3 != null ? osMotilidad3.hashCode() : 0);
        result = 31 * result + (osViasLagrimales != null ? osViasLagrimales.hashCode() : 0);
        result = 31 * result + (osParpadosPestanas != null ? osParpadosPestanas.hashCode() : 0);
        result = 31 * result + (osConjuntiva != null ? osConjuntiva.hashCode() : 0);
        result = 31 * result + (osEsclera != null ? osEsclera.hashCode() : 0);
        result = 31 * result + (osCornea != null ? osCornea.hashCode() : 0);
        result = 31 * result + (osCamaraAnterior != null ? osCamaraAnterior.hashCode() : 0);
        result = 31 * result + (osIris != null ? osIris.hashCode() : 0);
        result = 31 * result + (osPupila != null ? osPupila.hashCode() : 0);
        result = 31 * result + (osTio != null ? osTio.hashCode() : 0);
        result = 31 * result + (osGonioscopia != null ? osGonioscopia.hashCode() : 0);
        result = 31 * result + (osCristalino != null ? osCristalino.hashCode() : 0);
        result = 31 * result + (osVitreo != null ? osVitreo.hashCode() : 0);
        result = 31 * result + (osRetina != null ? osRetina.hashCode() : 0);
        result = 31 * result + (osNervioOptico != null ? osNervioOptico.hashCode() : 0);
        result = 31 * result + (osOrbita != null ? osOrbita.hashCode() : 0);
        result = 31 * result + (osObservaciones != null ? osObservaciones.hashCode() : 0);
        result = 31 * result + (diagnostico1KeyDiagnosticoconductaId != null ? diagnostico1KeyDiagnosticoconductaId.hashCode() : 0);
        result = 31 * result + (diagnosticoconductaDiagnostico1KeyOrganoId != null ? diagnosticoconductaDiagnostico1KeyOrganoId.hashCode() : 0);
        result = 31 * result + (diagnostico2KeyDiagnosticoconductaId != null ? diagnostico2KeyDiagnosticoconductaId.hashCode() : 0);
        result = 31 * result + (diagnosticoconductaDiagnostico2KeyOrganoId != null ? diagnosticoconductaDiagnostico2KeyOrganoId.hashCode() : 0);
        result = 31 * result + (diagnostico3KeyDiagnosticoconductaId != null ? diagnostico3KeyDiagnosticoconductaId.hashCode() : 0);
        result = 31 * result + (diagnosticoconductaDiagnostico3KeyOrganoId != null ? diagnosticoconductaDiagnostico3KeyOrganoId.hashCode() : 0);
        result = 31 * result + (diagnostico4KeyDiagnosticoconductaId != null ? diagnostico4KeyDiagnosticoconductaId.hashCode() : 0);
        result = 31 * result + (diagnosticoconductaDiagnostico4KeyOrganoId != null ? diagnosticoconductaDiagnostico4KeyOrganoId.hashCode() : 0);
        result = 31 * result + (diagnostico5KeyDiagnosticoconductaId != null ? diagnostico5KeyDiagnosticoconductaId.hashCode() : 0);
        result = 31 * result + (diagnosticoconductaDiagnostico5KeyOrganoId != null ? diagnosticoconductaDiagnostico5KeyOrganoId.hashCode() : 0);
        result = 31 * result + (diagnosticoObservacionesConducta != null ? diagnosticoObservacionesConducta.hashCode() : 0);
        result = 31 * result + (diagnosticoConductaTexto != null ? diagnosticoConductaTexto.hashCode() : 0);
        result = 31 * result + (mdEstadoOperaciones != null ? mdEstadoOperaciones.hashCode() : 0);
        result = 31 * result + (mdQuirofano != null ? mdQuirofano.hashCode() : 0);
        result = 31 * result + (mdCirujano != null ? mdCirujano.hashCode() : 0);
        result = 31 * result + (keyTipoanestesiaId != null ? keyTipoanestesiaId.hashCode() : 0);
        result = 31 * result + (mdAnesteciologo != null ? mdAnesteciologo.hashCode() : 0);
        result = 31 * result + (keyCirugiaId != null ? keyCirugiaId.hashCode() : 0);
        result = 31 * result + (mdAyudante != null ? mdAyudante.hashCode() : 0);
        result = 31 * result + (mdHorainicio != null ? mdHorainicio.hashCode() : 0);
        result = 31 * result + (mdHorafinalizacion != null ? mdHorafinalizacion.hashCode() : 0);
        result = 31 * result + (keyLioId != null ? keyLioId.hashCode() : 0);
        result = 31 * result + (salaOperacionesObservaciones != null ? salaOperacionesObservaciones.hashCode() : 0);
        result = 31 * result + (salaOperacionesMotivo != null ? salaOperacionesMotivo.hashCode() : 0);
        result = 31 * result + (salaOperacionesDiagnostico != null ? salaOperacionesDiagnostico.hashCode() : 0);
        result = 31 * result + (esferaOd != null ? esferaOd.hashCode() : 0);
        result = 31 * result + (cilindroOd != null ? cilindroOd.hashCode() : 0);
        result = 31 * result + (ejeOd != null ? ejeOd.hashCode() : 0);
        result = 31 * result + (prismaOd != null ? prismaOd.hashCode() : 0);
        result = 31 * result + (adicionOd != null ? adicionOd.hashCode() : 0);
        result = 31 * result + (dipOd != null ? dipOd.hashCode() : 0);
        result = 31 * result + (esferaOi != null ? esferaOi.hashCode() : 0);
        result = 31 * result + (cilindroOi != null ? cilindroOi.hashCode() : 0);
        result = 31 * result + (ejeOi != null ? ejeOi.hashCode() : 0);
        result = 31 * result + (prismaOi != null ? prismaOi.hashCode() : 0);
        result = 31 * result + (adicionOi != null ? adicionOi.hashCode() : 0);
        result = 31 * result + (dipOi != null ? dipOi.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "od_ph_key_agudezavisual_id", nullable = true)
    public String getOdPhKeyAgudezavisualId() {
        return odPhKeyAgudezavisualId;
    }

    public void setOdPhKeyAgudezavisualId(String odPhKeyAgudezavisualId) {
        this.odPhKeyAgudezavisualId = odPhKeyAgudezavisualId;
    }

    @Basic
    @Column(name = "od_ph_calificacion_agudezavisual_id", nullable = true)
    public String getOdPhCalificacionAgudezavisualId() {
        return odPhCalificacionAgudezavisualId;
    }

    public void setOdPhCalificacionAgudezavisualId(String odPhCalificacionAgudezavisualId) {
        this.odPhCalificacionAgudezavisualId = odPhCalificacionAgudezavisualId;
    }

    @Basic
    @Column(name = "os_ph_key_agudezavisual_id", nullable = true)
    public String getOsPhKeyAgudezavisualId() {
        return osPhKeyAgudezavisualId;
    }

    public void setOsPhKeyAgudezavisualId(String osPhKeyAgudezavisualId) {
        this.osPhKeyAgudezavisualId = osPhKeyAgudezavisualId;
    }

    @Basic
    @Column(name = "os_ph_calificacion_agudezavisual_id", nullable = true)
    public String getOsPhCalificacionAgudezavisualId() {
        return osPhCalificacionAgudezavisualId;
    }

    public void setOsPhCalificacionAgudezavisualId(String osPhCalificacionAgudezavisualId) {
        this.osPhCalificacionAgudezavisualId = osPhCalificacionAgudezavisualId;
    }

    @Basic
    @Column(name = "decerca_ph_key_agudezavisual_id", nullable = true)
    public String getDecercaPhKeyAgudezavisualId() {
        return decercaPhKeyAgudezavisualId;
    }

    public void setDecercaPhKeyAgudezavisualId(String decercaPhKeyAgudezavisualId) {
        this.decercaPhKeyAgudezavisualId = decercaPhKeyAgudezavisualId;
    }

    @Basic
    @Column(name = "decerca_ph_calificacion_agudezavisual_id", nullable = true)
    public String getDecercaPhCalificacionAgudezavisualId() {
        return decercaPhCalificacionAgudezavisualId;
    }

    public void setDecercaPhCalificacionAgudezavisualId(String decercaPhCalificacionAgudezavisualId) {
        this.decercaPhCalificacionAgudezavisualId = decercaPhCalificacionAgudezavisualId;
    }

    @Basic
    @Column(name = "diagnosticoconducta_tratamientos", nullable = true, length = 240)
    public String getDiagnosticoconductaTratamientos() {
        return diagnosticoconductaTratamientos;
    }

    public void setDiagnosticoconductaTratamientos(String diagnosticoconductaTratamientos) {
        this.diagnosticoconductaTratamientos = diagnosticoconductaTratamientos;
    }

    @Basic
    @Column(name = "sala_operaciones_complicaciones", nullable = true)
    public String getSalaOperacionesComplicaciones() {
        return salaOperacionesComplicaciones;
    }

    public void setSalaOperacionesComplicaciones(String salaOperacionesComplicaciones) {
        this.salaOperacionesComplicaciones = salaOperacionesComplicaciones;
    }

    @Basic
    @Column(name = "altura_OD", nullable = true)
    public String getAlturaOd() {
        return alturaOd;
    }

    public void setAlturaOd(String alturaOd) {
        this.alturaOd = alturaOd;
    }

    @Basic
    @Column(name = "altura_OI", nullable = true)
    public String getAlturaOi() {
        return alturaOi;
    }

    public void setAlturaOi(String alturaOi) {
        this.alturaOi = alturaOi;
    }

    @Basic
    @Column(name = "od_pio", nullable = true)
    public String getOdPio() {
        return odPio;
    }

    public void setOdPio(String odPio) {
        this.odPio = odPio;
    }

    @Basic
    @Column(name = "os_pio", nullable = true)
    public String getOsPio() {
        return osPio;
    }

    public void setOsPio(String osPio) {
        this.osPio = osPio;
    }

    @Basic
    @Column(name = "decerca_pio", nullable = true)
    public String getDecercaPio() {
        return decercaPio;
    }

    public void setDecercaPio(String decercaPio) {
        this.decercaPio = decercaPio;
    }

    @Basic
    @Column(name = "od_emetropia", nullable = true)
    public String getOdEmetropia() {
        return odEmetropia;
    }

    public void setOdEmetropia(String odEmetropia) {
        this.odEmetropia = odEmetropia;
    }

    @Basic
    @Column(name = "os_emetropia", nullable = true)
    public String getOsEmetropia() {
        return osEmetropia;
    }

    public void setOsEmetropia(String osEmetropia) {
        this.osEmetropia = osEmetropia;
    }

    @Basic
    @Column(name = "decerca_emetropia", nullable = true)
    public String getDecercaEmetropia() {
        return decercaEmetropia;
    }

    public void setDecercaEmetropia(String decercaEmetropia) {
        this.decercaEmetropia = decercaEmetropia;
    }
}
