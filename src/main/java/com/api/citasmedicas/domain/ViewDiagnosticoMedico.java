package com.api.citasmedicas.domain;

/**
 * Created by Usuario on 14/12/2020.
 */
public interface ViewDiagnosticoMedico {

    String getPacienteId();

    String getMdTipoConsulta();

    String getNombrePaciente();

    String getSexo();

    String getConductaObservaciones();

    String getConductaConducta();

    String getConductaTratamiento();

    String getFechaProceso();

    String getMedico();

    String getHoraCita();

    String getClinica();
}
