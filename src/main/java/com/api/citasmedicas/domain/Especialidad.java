package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Usuario on 26/11/2020.
 */
@Entity
@Table(name = "md_especialidad", schema = "db_citasmedicas")
public class Especialidad {
    private String especialidadId;
    private String auProgramaInserta;
    private Timestamp auFechaRegistro;

    @Id
    @Column(name = "KEY_ESPECIALIDAD_ID", nullable = false)
    public String getEspecialidadId() {
        return especialidadId;
    }

    public void setEspecialidadId(String especialidadId) {
        this.especialidadId = especialidadId;
    }

    @Basic
    @Column(name = "AU_programa_inserta", nullable = false)
    public String getAuProgramaInserta() {
        return auProgramaInserta;
    }

    public void setAuProgramaInserta(String auProgramaInserta) {
        this.auProgramaInserta = auProgramaInserta;
    }

    @Basic
    @Column(name = "AU_FECHA_REGISTRO", nullable = false)
    public Timestamp getAuFechaRegistro() {
        return auFechaRegistro;
    }

    public void setAuFechaRegistro(Timestamp auFechaRegistro) {
        this.auFechaRegistro = auFechaRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Especialidad that = (Especialidad) o;

        if (especialidadId != null ? !especialidadId.equals(that.especialidadId) : that.especialidadId != null)
            return false;
        if (auProgramaInserta != null ? !auProgramaInserta.equals(that.auProgramaInserta) : that.auProgramaInserta != null)
            return false;
        return auFechaRegistro != null ? auFechaRegistro.equals(that.auFechaRegistro) : that.auFechaRegistro == null;
    }

    @Override
    public int hashCode() {
        int result = especialidadId != null ? especialidadId.hashCode() : 0;
        result = 31 * result + (auProgramaInserta != null ? auProgramaInserta.hashCode() : 0);
        result = 31 * result + (auFechaRegistro != null ? auFechaRegistro.hashCode() : 0);
        return result;
    }
}
