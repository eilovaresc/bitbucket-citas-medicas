package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Usuario on 6/7/2021.
 */
@Repository
public interface DiagnosticoFotosDoctorDetRepository extends JpaRepository<DiagnosticosFotosDoctorDet, Long> {

    DiagnosticosFotosDoctorDet save(DiagnosticosFotosDoctorDet s);

    @Query(value = "SELECT detalle_id as detalleId, a.foto_id as fotoId, foto,  \n" +
            "a.tipo as tipo, \n" +
            "a.nombre_imagen as nombre \n" +
            "FROM db_citasfotosdoctor.md_diagnosticosfotosdet a  \n" +
            "where detalle_id=:detalleId ", nativeQuery = true)
    ViewDiagnosticoFotosDoctor getFotoDoctorDetalleId(@Param("detalleId") Long detalleId);

}
