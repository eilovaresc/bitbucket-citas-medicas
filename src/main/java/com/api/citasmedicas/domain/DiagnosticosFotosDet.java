package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by Usuario on 6/7/2021.
 */
@Entity
@Table(name = "md_diagnosticosfotosdet", schema = "db_citasfotos", catalog = "db_citasfotos")
public class DiagnosticosFotosDet {
    private int detalleId;
    private int fotoId;
    private byte[] foto;
    private String ojo;
    private String tipo;
    private String nombreImagen;

    @Basic
    @Column(name = "nombre_imagen", nullable = true)
    public String getNombreImagen() {
        return nombreImagen;
    }

    public void setNombreImagen(String nombreImagen) {
        this.nombreImagen = nombreImagen;
    }

    @Basic
    @Column(name = "tipo", nullable = true)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Id
    @Column(name = "detalle_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getDetalleId() {
        return detalleId;
    }

    public void setDetalleId(int detalleId) {
        this.detalleId = detalleId;
    }

    @Basic
    @Column(name = "foto_id", nullable = false)
    public int getFotoId() {
        return fotoId;
    }

    public void setFotoId(int fotoId) {
        this.fotoId = fotoId;
    }

    @Basic
    @Column(name = "foto", nullable = true)
    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    @Basic
    @Column(name = "ojo", nullable = true)
    public String getOjo() {
        return ojo;
    }

    public void setOjo(String ojo) {
        this.ojo = ojo;
    }

    @Basic
    @Column(name = "fecha_registro")
    private Date fechaRegistro;

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DiagnosticosFotosDet that = (DiagnosticosFotosDet) o;

        if (detalleId != that.detalleId) return false;
        if (fotoId != that.fotoId) return false;
        if (!Arrays.equals(foto, that.foto)) return false;
        if (ojo != null ? !ojo.equals(that.ojo) : that.ojo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = detalleId;
        result = 31 * result + fotoId;
        result = 31 * result + Arrays.hashCode(foto);
        result = 31 * result + (ojo != null ? ojo.hashCode() : 0);
        return result;
    }
}
