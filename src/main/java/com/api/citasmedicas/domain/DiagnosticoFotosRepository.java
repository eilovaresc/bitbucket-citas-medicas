package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 3/7/2021.
 */

@Repository
public interface DiagnosticoFotosRepository extends JpaRepository<Diagnosticosfotos, Long> {

    Diagnosticosfotos save(Diagnosticosfotos s);

    List<Diagnosticosfotos> findByPacienteIdOrderByFechaRegistroDesc(String pacienteId);
}
