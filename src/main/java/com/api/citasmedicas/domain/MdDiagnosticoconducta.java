package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Usuario on 6/12/2020.
 */
@Entity
@Table(name = "md_diagnosticoconducta", schema = "db_citasmedicas")
public class MdDiagnosticoconducta {
    private String keyDiagnosticoConductaId;
    private String auProgramaInserta;
    private Timestamp auFechaRegistro;
    private String codigoInternacional;

    @Id
    @Column(name = "key_diagnosticoconducta_id", nullable = false, length = 160)
    public String getKeyDiagnosticoConductaId() {
        return keyDiagnosticoConductaId;
    }

    public void setKeyDiagnosticoConductaId(String keyDiagnosticoConductaId) {
        this.keyDiagnosticoConductaId = keyDiagnosticoConductaId;
    }

    @Basic
    @Column(name = "AU_programa_inserta", nullable = false, length = 20)
    public String getAuProgramaInserta() {
        return auProgramaInserta;
    }

    public void setAuProgramaInserta(String auProgramaInserta) {
        this.auProgramaInserta = auProgramaInserta;
    }

    @Basic
    @Column(name = "AU_FECHA_REGISTRO", nullable = false)
    public Timestamp getAuFechaRegistro() {
        return auFechaRegistro;
    }

    public void setAuFechaRegistro(Timestamp auFechaRegistro) {
        this.auFechaRegistro = auFechaRegistro;
    }

    @Basic
    @Column(name = "codigo_internacional", nullable = false, length = 20)
    public String getCodigoInternacional() {
        return codigoInternacional;
    }

    public void setCodigoInternacional(String codigoInternacional) {
        this.codigoInternacional = codigoInternacional;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MdDiagnosticoconducta that = (MdDiagnosticoconducta) o;

        if (keyDiagnosticoConductaId != null ? !keyDiagnosticoConductaId.equals(that.keyDiagnosticoConductaId) : that.keyDiagnosticoConductaId != null)
            return false;
        if (auProgramaInserta != null ? !auProgramaInserta.equals(that.auProgramaInserta) : that.auProgramaInserta != null)
            return false;
        if (auFechaRegistro != null ? !auFechaRegistro.equals(that.auFechaRegistro) : that.auFechaRegistro != null)
            return false;
        if (codigoInternacional != null ? !codigoInternacional.equals(that.codigoInternacional) : that.codigoInternacional != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = keyDiagnosticoConductaId != null ? keyDiagnosticoConductaId.hashCode() : 0;
        result = 31 * result + (auProgramaInserta != null ? auProgramaInserta.hashCode() : 0);
        result = 31 * result + (auFechaRegistro != null ? auFechaRegistro.hashCode() : 0);
        result = 31 * result + (codigoInternacional != null ? codigoInternacional.hashCode() : 0);
        return result;
    }
}
