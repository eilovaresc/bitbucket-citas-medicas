package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 13/11/2020.
 */
@Repository
public interface CitaPacienteRepository extends JpaRepository<CitaPaciente, String> {


    @Query(value = "select a.KEY_Paciente_ID pacienteId, " +
            "b.md_nombre_paciente nombrePaciente, " +
            "a.KEY_CLINICA_ID clinicaId, " +
            "KEY_USER_MEDICO_ID medicoId, " +
            "d.MD_NOMBRE_USUARIO medicoNombre, " +
            "KEY_ESPECIALIDAD_ID especialidad, " +
            "KEY_PADECIMIENTO_ID padecimiento, " +
            "MD_FECHA_PLANIFICADA fechaPlanifica, " +
            "MD_HORA_PLANIFICADA horaPlanifica, " +
            "MD_ESTADO_CITA estado from md_citaspacientes a " +
            "left join md_pacientes b on a.KEY_Paciente_ID=b.KEY_Paciente_ID " +
            "left join md_users d on d.KEY_USER_ID=a.KEY_USER_MEDICO_ID " +
            "where MD_FECHA_PLANIFICADA=:fechaPlanifica and a.KEY_CLINICA_ID=:clinicaId order by MD_HORA_PLANIFICADA", nativeQuery = true)
    List<ViewCitaPaciente> findByFechaPlanifica(@Param("fechaPlanifica") String fechaPlanifica,
                                                @Param("clinicaId") String clinicaId);


    @Query(value = "select a.KEY_Paciente_ID pacienteId, " +
            "b.md_nombre_paciente nombrePaciente, " +
            "a.KEY_CLINICA_ID clinicaId, " +
            "KEY_USER_MEDICO_ID medicoId, " +
            "d.MD_NOMBRE_USUARIO medicoNombre, " +
            "KEY_ESPECIALIDAD_ID especialidad, " +
            "KEY_PADECIMIENTO_ID padecimiento, " +
            "MD_FECHA_PLANIFICADA fechaPlanifica, " +
            "MD_HORA_PLANIFICADA horaPlanifica, " +
            "MD_ESTADO_CITA estado, " +
            "b.md_sexo sexo, " +
            "floor(datediff(CURDATE(), b.MD_FECHA_NACIMIENTO)/ 365.25) edad, " +
            "c.MD_DESCRIPCION as clinicaNombre, " +
            "a.MD_TIPO_CONSULTA as tipoConsulta," +
            "case when MD_GLICEMIA='S' then 'SI' else 'NO' end as glicemia, " +
            "md_factura  as factura " +
            "from md_citaspacientes a " +
            "left join md_pacientes b on a.KEY_Paciente_ID=b.KEY_Paciente_ID " +
            "left join md_clinicas c on b.key_clinica_id=c.KEY_CLINICA_ID " +
            "left join md_users d on d.KEY_USER_ID=a.KEY_USER_MEDICO_ID " +
            "where a.MD_ESTADO_CITA='A' AND  MD_FECHA_PLANIFICADA=:fechaPlanifica and a.KEY_CLINICA_ID=:clinicaId and a.KEY_USER_MEDICO_ID=:medicoId order by MD_HORA_PLANIFICADA", nativeQuery = true)
    List<ViewCitaPaciente> findByClinicaIdMedicoIdFechaPlanifica(@Param("fechaPlanifica") String fechaPlanifica,
                                                                 @Param("clinicaId") String clinicaId,
                                                                 @Param("medicoId") String medicoId);


    @Query(value = "select a.KEY_Paciente_ID pacienteId, " +
            "b.md_nombre_paciente nombrePaciente, " +
            "a.KEY_CLINICA_ID clinicaId, " +
            "KEY_USER_MEDICO_ID medicoId, " +
            "d.MD_NOMBRE_USUARIO medicoNombre, " +
            "KEY_ESPECIALIDAD_ID especialidad, " +
            "KEY_PADECIMIENTO_ID padecimiento, " +
            "MD_FECHA_PLANIFICADA fechaPlanifica, " +
            "MD_HORA_PLANIFICADA horaPlanifica, " +
            "MD_ESTADO_CITA estado, " +
            "b.md_sexo sexo, " +
            "floor(datediff(CURDATE(), b.MD_FECHA_NACIMIENTO)/ 365.25) edad, " +
            "c.MD_DESCRIPCION as clinicaNombre, " +
            "a.MD_TIPO_CONSULTA as tipoConsulta " +
            "from md_citaspacientes a " +
            "left join md_pacientes b on a.KEY_Paciente_ID=b.KEY_Paciente_ID " +
            "left join md_clinicas c on b.key_clinica_id=c.KEY_CLINICA_ID " +
            "left join md_users d on d.KEY_USER_ID=a.KEY_USER_MEDICO_ID " +
            "where a.MD_ESTADO_CITA='A' AND  MD_FECHA_PLANIFICADA=:fechaPlanifica and a.KEY_CLINICA_ID=:clinicaId order by MD_HORA_PLANIFICADA", nativeQuery = true)
    List<ViewCitaPaciente> findByClinicaIdFechaPlanifica(@Param("fechaPlanifica") String fechaPlanifica,
                                                                 @Param("clinicaId") String clinicaId);

    CitaPaciente findByPacienteId(String pacienteId);

    CitaPaciente findByPacienteIdAndFechaPlanificaAndHoraPlanifica(String pacienteId, String fechaPlanifica, String horaPlanifica);

}
