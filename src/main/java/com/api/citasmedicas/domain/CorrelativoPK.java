package com.api.citasmedicas.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Usuario on 20/3/2021.
 */
public class CorrelativoPK implements Serializable {
    private int anio;
    private int mes;

    @Column(name = "anio", nullable = false)
    @Id
    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    @Column(name = "mes", nullable = false)
    @Id
    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CorrelativoPK that = (CorrelativoPK) o;

        if (anio != that.anio) return false;
        if (mes != that.mes) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = anio;
        result = 31 * result + mes;
        return result;
    }
}
