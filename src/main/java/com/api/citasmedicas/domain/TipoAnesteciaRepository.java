package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 7/12/2020.
 */
@Repository
public interface TipoAnesteciaRepository extends JpaRepository<TipoAnestesia, String> {

    List<TipoAnestesia> findAll();
}
