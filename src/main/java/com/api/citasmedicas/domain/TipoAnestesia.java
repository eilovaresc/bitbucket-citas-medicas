package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Usuario on 7/12/2020.
 */
@Entity
@Table(name = "md_tiposanestesias", schema = "db_citasmedicas")
public class TipoAnestesia {
    private String tipoAnestesiaId;
    private String mdDescrihionAnestesia;
    private String programaInserta;
    private Timestamp fechaRegistro;

    @Id
    @Column(name = "KEY_TIPOANESTESIA_ID", nullable = false, length = 10)
    public String getTipoAnestesiaId() {
        return tipoAnestesiaId;
    }

    public void setTipoAnestesiaId(String tipoAnestesiaId) {
        this.tipoAnestesiaId = tipoAnestesiaId;
    }

    @Basic
    @Column(name = "MD_DESCRIHION_ANESTESIA", nullable = false, length = 50)
    public String getMdDescrihionAnestesia() {
        return mdDescrihionAnestesia;
    }

    public void setMdDescrihionAnestesia(String mdDescrihionAnestesia) {
        this.mdDescrihionAnestesia = mdDescrihionAnestesia;
    }

    @Basic
    @Column(name = "AU_programa_inserta", nullable = false, length = 20)
    public String getProgramaInserta() {
        return programaInserta;
    }

    public void setProgramaInserta(String programaInserta) {
        this.programaInserta = programaInserta;
    }

    @Basic
    @Column(name = "AU_FECHA_REGISTRO", nullable = false)
    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TipoAnestesia that = (TipoAnestesia) o;

        if (tipoAnestesiaId != null ? !tipoAnestesiaId.equals(that.tipoAnestesiaId) : that.tipoAnestesiaId != null)
            return false;
        if (mdDescrihionAnestesia != null ? !mdDescrihionAnestesia.equals(that.mdDescrihionAnestesia) : that.mdDescrihionAnestesia != null)
            return false;
        if (programaInserta != null ? !programaInserta.equals(that.programaInserta) : that.programaInserta != null)
            return false;
        if (fechaRegistro != null ? !fechaRegistro.equals(that.fechaRegistro) : that.fechaRegistro != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tipoAnestesiaId != null ? tipoAnestesiaId.hashCode() : 0;
        result = 31 * result + (mdDescrihionAnestesia != null ? mdDescrihionAnestesia.hashCode() : 0);
        result = 31 * result + (programaInserta != null ? programaInserta.hashCode() : 0);
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        return result;
    }
}
