package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Usuario on 2/7/2021.
 */
@Entity
@Table(name = "md_diagnosticosfotos", schema = "db_citasfotos", catalog = "db_citasfotos")
public class Diagnosticosfotos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "foto_id", nullable = false)
    private long fotoId;

    @Basic
    @Column(name = "paciente_id", nullable = false)
    private String pacienteId;

    @Basic
    @Column(name = "observaciones_izquierdo")
    private String observacionesIzquierda;

    @Basic
    @Column(name = "observaciones_derecho")
    private String observacionesDerecha;

    @Basic
    @Column(name = "fecha_registro")
    private Date fechaRegistro;
    @Basic
    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public long getFotoId() {
        return fotoId;
    }

    public void setFotoId(long fotoId) {
        this.fotoId = fotoId;
    }

    public String getPacienteId() {
        return pacienteId;
    }

    public void setPacienteId(String pacienteId) {
        this.pacienteId = pacienteId;
    }

    public String getObservacionesIzquierda() {
        return observacionesIzquierda;
    }

    public void setObservacionesIzquierda(String observacionesIzquierda) {
        this.observacionesIzquierda = observacionesIzquierda;
    }

    public String getObservacionesDerecha() {
        return observacionesDerecha;
    }

    public void setObservacionesDerecha(String observacionesDerecha) {
        this.observacionesDerecha = observacionesDerecha;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Diagnosticosfotos)) return false;

        Diagnosticosfotos that = (Diagnosticosfotos) o;

        if (getFotoId() != that.getFotoId()) return false;
        if (getPacienteId() != null ? !getPacienteId().equals(that.getPacienteId()) : that.getPacienteId() != null)
            return false;
        if (getObservacionesIzquierda() != null ? !getObservacionesIzquierda().equals(that.getObservacionesIzquierda()) : that.getObservacionesIzquierda() != null)
            return false;
        if (getObservacionesDerecha() != null ? !getObservacionesDerecha().equals(that.getObservacionesDerecha()) : that.getObservacionesDerecha() != null)
            return false;
        return getFechaRegistro() != null ? getFechaRegistro().equals(that.getFechaRegistro()) : that.getFechaRegistro() == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (getFotoId() ^ (getFotoId() >>> 32));
        result = 31 * result + (getPacienteId() != null ? getPacienteId().hashCode() : 0);
        result = 31 * result + (getObservacionesIzquierda() != null ? getObservacionesIzquierda().hashCode() : 0);
        result = 31 * result + (getObservacionesDerecha() != null ? getObservacionesDerecha().hashCode() : 0);
        result = 31 * result + (getFechaRegistro() != null ? getFechaRegistro().hashCode() : 0);
        return result;
    }
}
