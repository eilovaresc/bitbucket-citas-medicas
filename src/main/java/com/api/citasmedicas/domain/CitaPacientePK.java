package com.api.citasmedicas.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Usuario on 3/3/2021.
 */
public class CitaPacientePK implements Serializable {

    @Id
    @Column(name = "KEY_Paciente_ID")
    private String pacienteId;
    @Id
    @Column(name = "MD_FECHA_PLANIFICADA")
    private String fechaPlanifica;
    @Id
    @Column(name = "MD_HORA_PLANIFICADA")
    private String horaPlanifica;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CitaPacientePK)) return false;

        CitaPacientePK that = (CitaPacientePK) o;

        if (pacienteId != null ? !pacienteId.equals(that.pacienteId) : that.pacienteId != null) return false;
        if (fechaPlanifica != null ? !fechaPlanifica.equals(that.fechaPlanifica) : that.fechaPlanifica != null)
            return false;
        return horaPlanifica != null ? horaPlanifica.equals(that.horaPlanifica) : that.horaPlanifica == null;
    }

    @Override
    public int hashCode() {
        int result = pacienteId != null ? pacienteId.hashCode() : 0;
        result = 31 * result + (fechaPlanifica != null ? fechaPlanifica.hashCode() : 0);
        result = 31 * result + (horaPlanifica != null ? horaPlanifica.hashCode() : 0);
        return result;
    }
}
