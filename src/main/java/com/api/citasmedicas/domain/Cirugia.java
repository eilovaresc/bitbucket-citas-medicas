package com.api.citasmedicas.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Usuario on 7/12/2020.
 */
@Entity
@Table(name = "md_cirugias", schema = "db_citasmedicas")
public class Cirugia {
    private String cirugiaId;
    private String programaInserta;
    private Timestamp afechaRegistro;

    @Id
    @Column(name = "KEY_CIRUGIA_ID", nullable = false)
    public String getCirugiaId() {
        return cirugiaId;
    }

    public void setCirugiaId(String cirugiaId) {
        this.cirugiaId = cirugiaId;
    }

    @Basic
    @Column(name = "AU_programa_inserta", nullable = false)
    public String getProgramaInserta() {
        return programaInserta;
    }

    public void setProgramaInserta(String programaInserta) {
        this.programaInserta = programaInserta;
    }

    @Basic
    @Column(name = "AU_FECHA_REGISTRO", nullable = false)
    public Timestamp getAfechaRegistro() {
        return afechaRegistro;
    }

    public void setAfechaRegistro(Timestamp afechaRegistro) {
        this.afechaRegistro = afechaRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cirugia cirugia = (Cirugia) o;

        if (cirugiaId != null ? !cirugiaId.equals(cirugia.cirugiaId) : cirugia.cirugiaId != null) return false;
        if (programaInserta != null ? !programaInserta.equals(cirugia.programaInserta) : cirugia.programaInserta != null)
            return false;
        if (afechaRegistro != null ? !afechaRegistro.equals(cirugia.afechaRegistro) : cirugia.afechaRegistro != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cirugiaId != null ? cirugiaId.hashCode() : 0;
        result = 31 * result + (programaInserta != null ? programaInserta.hashCode() : 0);
        result = 31 * result + (afechaRegistro != null ? afechaRegistro.hashCode() : 0);
        return result;
    }
}
