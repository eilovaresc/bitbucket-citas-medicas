package com.api.citasmedicas.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Usuario on 13/11/2020.
 */
@Entity
@Table(name = "md_clinicas")
public class Clinica {

    @Id
    @Column(name = "KEY_CLINICA_ID")
    private String clinicaId;
    @Column(name = "MD_DESCRIPCION")
    private String descripcion;
    @Column(name = "MD_programa_inserta")
    private String programaInserta;
    @Column(name = "MD_FECHA_REGISTRO")
    private Date fechaRegistro;
    @Column(name = "md_ultimo_correlativo")
    private Integer correlativo;
    @Column(name = "MD_COD_CLINICA")
    private Integer codClinica;

    public Integer getCodClinica() {
        return codClinica;
    }

    public void setCodClinica(Integer codClinica) {
        this.codClinica = codClinica;
    }

    public String getClinicaId() {
        return clinicaId;
    }

    public void setClinicaId(String clinicaId) {
        this.clinicaId = clinicaId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getProgramaInserta() {
        return programaInserta;
    }

    public void setProgramaInserta(String programaInserta) {
        this.programaInserta = programaInserta;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(Integer correlativo) {
        this.correlativo = correlativo;
    }
}
