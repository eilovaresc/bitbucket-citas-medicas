package com.api.citasmedicas.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Usuario on 15/11/2020.
 */
@Entity
@Table(name = "md_users")
public class Usuario {
    @Id
    @Column(name = "KEY_USER_ID")
    private String usuarioId;
    @Column(name = "MD_NOMBRE_USUARIO")
    private String nombre;
    @Column(name = "KEY_CLINICA_ID")
    private String clinicaId;
    @Column(name = "KEY_ROL_ID")
    private String rol;
    @Column(name = "AU_programa_inserta")
    private String programaInserta;
    @Column(name = "AU_FECHA_REGISTRO")
    private Date fechaResitro;
    @Column(name = "password_user")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClinicaId() {
        return clinicaId;
    }

    public void setClinicaId(String clinicaId) {
        this.clinicaId = clinicaId;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getProgramaInserta() {
        return programaInserta;
    }

    public void setProgramaInserta(String programaInserta) {
        this.programaInserta = programaInserta;
    }

    public Date getFechaResitro() {
        return fechaResitro;
    }

    public void setFechaResitro(Date fechaResitro) {
        this.fechaResitro = fechaResitro;
    }
}
