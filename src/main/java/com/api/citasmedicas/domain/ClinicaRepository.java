package com.api.citasmedicas.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Usuario on 13/11/2020.
 */
@Repository
public interface ClinicaRepository extends JpaRepository<Clinica, String> {
    Clinica findByClinicaId(String clinicaId);

    List<Clinica> findAll();
}
