package com.api.citasmedicas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class CitasMedicasApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(CitasMedicasApplication.class, args);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/*").allowedOrigins("*");
                registry.addMapping("/usuario/rol/*").allowedOrigins("*");
                registry.addMapping("/usuario/login").allowedOrigins("*");
                registry.addMapping("/paciente/*").allowedOrigins("*");
                registry.addMapping("/cita-paciente/fecha-cita/*").allowedOrigins("*");
                registry.addMapping("/cita-paciente/medico/*/*").allowedOrigins("*");
                registry.addMapping("/cita-paciente/registro/*").allowedOrigins("*");
                registry.addMapping("/cita-paciente/agendar").allowedOrigins("*");
                registry.addMapping("/clinica/*").allowedOrigins("*");
                registry.addMapping("/padecimientos").allowedOrigins("*");
                registry.addMapping("/especialidades").allowedOrigins("*");
                registry.addMapping("/diagnostico-medico").allowedOrigins("*");
                registry.addMapping("/diagnostico-agudeza/*").allowedOrigins("*");
                registry.addMapping("/diagnostico-conducta/*").allowedOrigins("*");

                registry.addMapping("/diagnostico-fotos/upload/izquierdo").allowedOrigins("*");
                registry.addMapping("/diagnostico-fotos/upload/derecho").allowedOrigins("*");
                registry.addMapping("/diagnostico-fotos/medico/upload").allowedOrigins("*");
                registry.addMapping("/diagnostico-fotos/paciente/*").allowedOrigins("*");
                registry.addMapping("/diagnostico-fotos/medico/paciente/*/*/*").allowedOrigins("*");

                registry.addMapping("/diagnostico-fotos/medico/*").allowedOrigins("*");
                registry.addMapping("/diagnostico-fotos/detalle/delete").allowedOrigins("*");

                registry.addMapping("/diagnostico-lio/*").allowedOrigins("*");
                registry.addMapping("/agudeza-visual/*").allowedOrigins("*");
                registry.addMapping("/agudeza-adicional/*").allowedOrigins("*");
                registry.addMapping("/cirugia/*").allowedOrigins("*");
                registry.addMapping("/tipo-anestesia/*").allowedOrigins("*");
                registry.addMapping("/reportes/resumencontrolpacientes/*").allowedOrigins("*");
                registry.addMapping("/diagnostico-medico/paciente/*").allowedOrigins("*");
                registry.addMapping("/diagnostico-medico/paciente/query/*").allowedOrigins("*");
                registry.addMapping("/diagnostico-medico/paciente/*/fecha/*").allowedOrigins("*");

            }
        };
    }

}
